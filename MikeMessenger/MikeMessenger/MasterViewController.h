//
//  MasterViewController.h
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import <UIKit/UIKit.h>
#import "GuideViewController.h"

@interface MasterViewController : UIViewController<UIPageViewControllerDataSource>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageImages;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@end

