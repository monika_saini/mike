//
//  SettingsViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/17/15.
//
//

#import <UIKit/UIKit.h>
#import "SettingCell.h"
#import "ProfileDetailsViewController.h"
#import "PaymentInfoViewController.h"
#import "NotifiSoundViewController.h"
#import "ChatSettingsViewController.h"
#import "LoginViewController.h"
#import "DataClass.h"
#import "LoginViewController.h"
#import "FCAPIController.h"

@interface SettingsViewController : UIViewController<UIScrollViewDelegate>
{
    
    __weak IBOutlet UITableView *tblSettings;
    DataClass *dataClass_obj;
}

@property(nonatomic,retain)NSMutableArray *arrSettingData;
@property(nonatomic)NSInteger previousPage;
@property(nonatomic,retain)NSArray *arrImg;

@end
