//
//  ChatListViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/10/15.
//
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ChatListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate>
@property(nonatomic,strong)IBOutlet UITableView *tView;
@property (weak, nonatomic) IBOutlet UIButton *btncar;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *composeButton;

-(IBAction)editTableviewPessed:(id)sender;
- (IBAction)composePersonalChat:(id)sender;
@property (nonatomic, strong) NSMutableArray *arrContactsData;
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;
-(void)getCotact;
@end
