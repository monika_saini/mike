//
//  ISDCodeTableViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/11/15.
//
//

#import "ISDCodeTableViewController.h"
#define kCellID @"countryCodeCell"
@interface ISDCodeTableViewController ()
@property (nonatomic, strong) NSMutableArray *codesArray;
@end

@implementation ISDCodeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setUpDataSourceArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper methods
-(void)setUpDataSourceArray{
    //Get country ISD code list
    NSString *txtFilePath = [[NSBundle mainBundle] pathForResource:@"CountryCode" ofType:@"txt"];
    NSError *error;
    NSString *strFileContent = [NSString stringWithContentsOfFile:txtFilePath encoding:NSUTF8StringEncoding error:&error];
    if(error) {  //Handle error
        
    }else{
        self.codesArray=[NSMutableArray array];
        //NSLog(@"File content : %@ ", strFileContent);
        // first, separate by new line
        NSArray* allLinedStrings = [strFileContent componentsSeparatedByCharactersInSet:
                                    [NSCharacterSet newlineCharacterSet]];
        for (NSString *line in allLinedStrings) {
            NSArray *separatedComponents=[line componentsSeparatedByString:@":"];
            NSDictionary *cellContent = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[separatedComponents objectAtIndex:0],[separatedComponents lastObject], nil]
                                                                    forKeys:[NSArray arrayWithObjects:@"country",@"code", nil]];
            [self.codesArray addObject:cellContent];
        }
        [self.tableView reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.codesArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellID];
    }
    NSDictionary *cellContent = [self.codesArray objectAtIndex:indexPath.row];
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[cellContent objectForKey:@"country"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"+%@",[cellContent objectForKey:@"code"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellContent = [self.codesArray objectAtIndex:indexPath.row];   
    
    [self.delegate ISDCodeTableViewControllerDidSave:self withDict:cellContent];
}

#pragma mark - IBActions
- (IBAction)cancel:(id)sender
{
    [self.delegate ISDCodeTableViewControllerDidCancel:self];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
