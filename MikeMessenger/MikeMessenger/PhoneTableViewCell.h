//
//  PhoneTableViewCell.h
//  MikeMessenger
//
//  Created by Monika on 2/11/15.
//
//

#import <UIKit/UIKit.h>

@interface PhoneTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *ISDLabel;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;

@end
