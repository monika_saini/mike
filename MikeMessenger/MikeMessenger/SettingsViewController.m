//
//  SettingsViewController.m
//  MikeMessenger
//
//  Created by Dipak B on 2/17/15.
//
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize arrSettingData,arrImg;
- (void)viewDidLoad {
   
    dataClass_obj=[[DataClass alloc]init];
    arrSettingData=[[NSMutableArray alloc]initWithObjects:@"xyz",@"Payment Info",@"Profile details",@"Notification & Sounds",@"Privacy & Security",@"Chat Settings",@"Manage Accounts",@"Images",@"Archive all conversation",@"Clear all conversation",nil];
arrImg=[[NSArray alloc]initWithObjects:@"image1_",@"image2_",@"image3_",nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.navigationItem.leftBarButtonItem=nil;
    self.tabBarController.navigationItem.hidesBackButton=YES;
    UIBarButtonItem *btnShare1 = [[UIBarButtonItem alloc]
                                  initWithTitle:@""
                                  style:UIBarButtonItemStyleBordered
                                  target:self
                                  action:@selector(share:)];
    
    self.tabBarController.navigationItem.rightBarButtonItems=[NSArray arrayWithObject:btnShare1];
    
    self.tabBarController.navigationItem.title=@"Settings";
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
       return 100;
    }
    else if(indexPath.row==7)
    {
        return 120;
    }else{
        return 40;
    }
    return 0.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"taskCell";
    SettingCell *cell = [tblSettings dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SettingCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];

    [cell.scrollView setHidden:YES];
    if (indexPath.row==0)
    {
         [cell.lblName setHidden:NO];
        [cell.lblDetailDescri setHidden:NO];
         cell.selectionStyle=UITableViewCellSelectionStyleNone;
      [cell.lblName setHidden:NO];
        [cell.lblProfileName setHidden:YES];
        //[cell.lblName setText:@"Dipak Bhadane"];
        NSString *fbName = [FCAPIController sharedInstance].currentUser.name;

        [cell.lblName setText:fbName];

        
        [cell.imgProfile setImage:[UIImage imageNamed:@"index1.jpg"]];
        cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.width/2;
        cell.imgProfile.clipsToBounds=YES;
       cell.imgProfile.layer.borderWidth = 2.0f;
       cell.imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
        [cell.lblProfileName setText:@"Dipak Patil"];
        [cell.lblDetailDescri setText:@"so happy today"];
        //cell.backgroundColor=[UIColor lightGrayColor];
      [cell setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.10]];
        
        
    }else if(indexPath.row==1)
    {
        [cell.lblName setHidden:YES];
       [cell.lblDetailDescri setHidden:YES];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        [cell.lblProfileName setText:[arrSettingData objectAtIndex:indexPath.row]];
    }else if(indexPath.row==7)
    {
        [cell.lblName setHidden:YES];
        [cell.lblDetailDescri setHidden:YES];
        [cell.scrollView setHidden:NO];
        [cell.lblProfileName setHidden:YES];
        [cell.lblName setHidden:YES];
        [cell.lblDetailDescri setHidden:YES];
      cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        NSLog(@"%lu",(unsigned long)arrImg.count);
        
        for (int i=0; i<arrImg.count; i++)
        {
            
            
            UIImageView *imageview=[[UIImageView alloc] init];
            
            [imageview setFrame:CGRectMake(0,10, 69, 90)];
            imageview.clipsToBounds=YES;
            imageview.layer.masksToBounds=YES;
            [imageview setImage:[UIImage imageNamed:[arrImg objectAtIndex:i]]];
            imageview.tag=i;
            [cell.scrollView addSubview:imageview];
            [imageview setFrame:CGRectMake(100*i+(320-320),0, 69,90)];
            
            imageview.contentMode=UIViewContentModeScaleAspectFit;
            [imageview setUserInteractionEnabled:YES];
            [cell.scrollView setFrame:CGRectMake(0, 0, 320,90)];
            
            
            [cell.scrollView setDelegate:self];
            
            [cell.scrollView setContentSize:CGSizeMake(90*arrImg.count,90)];
            [cell.scrollView setContentOffset:CGPointMake(0, 0)];
            //[cell setFrame:CGRectMake(10,10, 320, 150)];

        }
    }else if(indexPath.row==8)
    {
        [cell.lblName setHidden:YES];
        [cell.lblDetailDescri setHidden:YES];
        [cell.lblName setHidden:YES];
        [cell.lblProfileName setHidden:NO];
        [cell.lblProfileName setText:[arrSettingData objectAtIndex:indexPath.row]];
        
    }else if(indexPath.row==9)
    {
        [cell.lblName setHidden:YES];
        [cell.lblDetailDescri setHidden:YES];
        [cell.lblName setHidden:YES];
        [cell.lblProfileName setHidden:NO];
        [cell.lblProfileName setText:[arrSettingData objectAtIndex:indexPath.row]];
        
    }else
    {
        [cell.lblName setHidden:YES];
        [cell.lblDetailDescri setHidden:YES];
        [cell.lblName setHidden:YES];
        [cell.lblProfileName setHidden:NO];
        [cell.lblProfileName setText:[arrSettingData objectAtIndex:indexPath.row]];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
        [self performSegueWithIdentifier:@"Go-PaymentInfo" sender:self];
             break;
        case 2:
            [self performSegueWithIdentifier:@"Go-Profile" sender:self];
           
            break;
        case 3:
             [self performSegueWithIdentifier:@"Go-NotifySound" sender:self];
            break;
        case 4:
            
            break;
        case 5:
            [self performSegueWithIdentifier:@"Go-ChatSetting" sender:self];
            break;
            case 6:
            {
            NSArray *viewControllers = [[self navigationController] viewControllers];
            for( int i=0;i<[viewControllers count];i++){
                id obj=[viewControllers objectAtIndex:i];
                if([obj isKindOfClass:[LoginViewController class]]){
                    [[self navigationController] popToViewController:obj animated:YES];
                    return;
                }
            }
        }
           // [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier]isEqualToString:@"Go-Profile"])
    {
        
         ProfileDetailsViewController *objProfileDetail=segue.destinationViewController;
        
       /* NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://hardcastlegis.co.in/Mike_Messenger/userDetail/getUserDetails.php"]];
        [request setHTTPMethod:@"POST"];
        
        NSString *mobileno=@"0123456789";
        NSString *userUpdate =[NSString stringWithFormat:@"MOBILE_NUM=%@",mobileno];
        NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
        
        //Apply the data to the body
        [request setHTTPBody:data1];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *resp,NSData *data,NSError *error)
         {
             NSString *txt = [[NSString alloc] initWithData:data encoding: NSASCIIStringEncoding];
             
             
             NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [txt dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingAllowFragments error: &error];
             if(error)
                 
                 NSLog(@"%@",error.description);
             else
                 
                 //objDataClass.m_DataDictionary=[JSON valueForKey:@"Data"];
                 
                 NSLog(@"Response : %@", JSON);
             
             
             dispatch_async(dispatch_get_main_queue(), ^{
                
        
                 objProfileDetail.data_Dict=[JSON valueForKey:@"data"];
                 
                 //[tblViewMembers reloadData];
                 //[self.ActivitIndicator stopAnimating];
                 //[self.ActivitIndicator setHidesWhenStopped:YES];
                 
             });
             
         }];
        

        */
    }
    if ([[segue identifier]isEqualToString:@"Go-PaymentInfo"]) {
        PaymentInfoViewController *objPayment=segue.destinationViewController;
    }
    if ([[segue identifier]isEqualToString:@"Go-NotifySound"]) {
        NotifiSoundViewController *objNotify=segue.destinationViewController;
    }
    
    if ([[segue identifier]isEqualToString:@"Go-ChatSetting"]) {
        NotifiSoundViewController *objNotify=segue.destinationViewController;
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
