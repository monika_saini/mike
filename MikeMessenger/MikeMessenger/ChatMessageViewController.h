//
//  ChatMessageViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/10/15.
//
//

#import <UIKit/UIKit.h>
#import "JSMessagesViewController.h"
#import "Conversation.h"

@interface ChatMessageViewController : JSMessagesViewController <JSMessagesViewDelegate, JSMessagesViewDataSource,UIActionSheetDelegate>
@property(nonatomic,assign) BOOL isMultipleUserChat;
@property (readwrite, nonatomic, strong) Conversation *conversation;
@property (nonatomic, strong) NSArray *quickSendContacts;

-(void)backPressed:(id)sender;
@end
