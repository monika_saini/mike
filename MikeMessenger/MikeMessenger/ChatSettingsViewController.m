//
//  ChatSettingsViewController.m
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import "ChatSettingsViewController.h"

@interface ChatSettingsViewController ()

@end

@implementation ChatSettingsViewController
@synthesize tblChatSettings,arrData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData=[[NSMutableArray alloc]initWithObjects:@"Chat  wallpaper",@"Media auto download",@"Save incoming media",@"Chat backup",@"Mike features", nil];
    // Do any additional setup after loading the view.
}
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.topItem.title = @"Back";
    
    /* [self.parentViewController.navigationController setNavigationBarHidden:YES];
   
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = item;
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@"CenturyGothic" size:18.0f], NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];*/
//[self.parentViewController.navigationController setNavigationBarHidden:YES];
    
    
}


- (IBAction)btnBack_Click:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotifyCell";
    notificationCell *cell = [tblChatSettings dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"notificationCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row==2) {
        [cell.onoff setHidden:NO];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }else if (indexPath.row==4){
        [cell.onoff setHidden:NO];
         cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }else{
        [cell.onoff setHidden:YES];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [cell.lblnotify setText:[arrData objectAtIndex:indexPath.row]];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            //[self performSegueWithIdentifier:@"Go-PaymentInfo" sender:self];
            break;
        case 2:
            //[self performSegueWithIdentifier:@"Go-Profile" sender:self];
            
            break;
        case 3:
            //[self performSegueWithIdentifier:@"Go-NotifySound" sender:self];
            break;
        case 4:
            
            break;
        case 5:
            // [self performSegueWithIdentifier:@"Go-ChatSetting" sender:self];
            break;
        case 6:
            // [self performSegueWithIdentifier:@"Go-ManageAccount" sender:self];
            break;
        default:
            break;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
