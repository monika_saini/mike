//
//  ChatMessageViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/10/15.
//
//

#import "ChatMessageViewController.h"
#import "Message.h"
#import "FCChatDataStoreManager.h"
#import "FCBaseChatRequestManager.h"
#import "FCAPIController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "XMPP.h"
#import "SDWebImageDownloader.h"
#import "FCUser.h"
#import "Sequencer.h"
#import "CustomBackView.h"
#import "UIImageView+WebCache.h"


@interface ChatMessageViewController ()
@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) UIImage *senderImage;
@property (nonatomic, strong) UIImage *reciverImage;
@end

@implementation ChatMessageViewController
@synthesize quickSendContacts;
@synthesize isMultipleUserChat;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=YES;
    CustomBackView *customView = [CustomBackView customView];
    
    if (self.quickSendContacts.count) {//quicksend back-button UI
        NSString *backString = @"To ";
        NSMutableArray *names = [NSMutableArray array];
        for(Conversation *conv in self.quickSendContacts){
            NSString *fullName = conv.facebookName;
            NSString *firstName = [[fullName componentsSeparatedByString:@" "] objectAtIndex:0];
            [names addObject:firstName];
        }
        backString = [backString stringByAppendingString:[names componentsJoinedByString:@","]];
        customView.contactNameLabel.text= backString;
        customView.statusLabel.hidden = YES;
       

    }else{

        customView.contactNameLabel.text=self.conversation.facebookName;
        NSString *url = [[NSString alloc]
                         initWithFormat:@"https://graph.facebook.com/%@/picture",self.conversation.facebookId];
        [customView.profilePicImageView setImageWithURL:[NSURL URLWithString:url]
                                       placeholderImage:nil
                                              completed:^(UIImage *image, NSError *error, SDImageCacheType type){}];
        customView.statusLabel.hidden = NO;
        

    }
    
    customView.profilePicImageView.layer.borderWidth = 1.0f;
    customView.profilePicImageView.layer.borderColor = [UIColor clearColor].CGColor;
    customView.profilePicImageView.layer.masksToBounds = NO;
    customView.profilePicImageView.clipsToBounds = YES;
    customView.profilePicImageView.layer.cornerRadius = 15;
    
    [customView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backPressed:)];
    [singleTap setNumberOfTapsRequired:1];
    [customView addGestureRecognizer:singleTap];
    
    //[self.customButton setCustomView:customView];
    UIBarButtonItem *customButton = [[UIBarButtonItem alloc] initWithCustomView:customView];
    [customButton setTarget:self];
    [customButton setAction:@selector(backPressed:)];

    self.navigationItem.leftBarButtonItem = customButton; // Add button to left button on bar
    
    if (self.isMultipleUserChat) {//group,broadcast,quick-send, no right bar-buttons
        self.navigationItem.rightBarButtonItems = nil;
    }else{
        //messenger and call button on right-top
        UIBarButtonItem *callItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"call"] style:(UIBarButtonItemStylePlain) target:self action:nil];
        
        UIButton *messengerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        messengerButton.frame = CGRectMake(0, 0, 20, 20);
        [messengerButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        messengerButton.showsTouchWhenHighlighted = YES;
        [messengerButton setBackgroundImage:[UIImage imageNamed:@"fb_message_icon.png"] forState:(UIControlStateNormal)];
        
        UIBarButtonItem *messengerItem = [[UIBarButtonItem alloc] initWithCustomView:messengerButton];
        
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:callItem,messengerItem,nil];
    }
    
    [self setUpSequencer];
    
}


- (IBAction)btnBack_Click:(id)sender {
    
    
     [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.quickSendContacts.count) {
        
    }else{
        self.conversation.badgeNumber = @(0);
        [[[FCAPIController sharedInstance] chatDataStoreManager] saveContext];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageReceived:)
                                                 name:kFCMessageDidComeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)backPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)setUpSequencer
{
    __weak ChatMessageViewController *self_ = self;
    Sequencer *sequencer = [Sequencer new];
    //1.fetch friend's pic
    if (self.quickSendContacts.count) {
        [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
            NSString *url = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",self_.conversation.facebookId];
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url]
                                                                  options:0
                                                                 progress:nil
                                                                completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                                                                    if (!error) {
                                                                        self_.senderImage = image;
                                                                    }
                                                                    completion(nil);
                                                                }];
        }];
    }
   
    //2.fetch current user's pic
    [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
        NSString *urlMine = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",[FCAPIController sharedInstance].currentUser.userId];
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlMine]
                                                              options:0
                                                             progress:nil
                                                            completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                                                                if (!error) {
                                                                    self_.reciverImage = image;
                                                                }
                                                                completion(nil);
                                                            }];
    }];
    
    //3. fetch all messages from local db(core-data)
    [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
        if (self.quickSendContacts.count) {//QUICK SEND, START A FRESH CHAT UI
           
            self_.messages = [NSMutableArray array];
            
        }else{//FETCH HISTORY ONLY FOR PERSONAL & GROUP CHAT
            self_.messages = [NSMutableArray arrayWithArray:[[[FCAPIController sharedInstance] chatDataStoreManager] fetchAllMessagesInConversation:self_.conversation]];
        }
        
       
        self_.delegate = self_;
        self_.dataSource = self_;
        [self_.tableView reloadData];
        completion(nil);
    }];
    
    [sequencer run];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return self.messages.count;
}


#pragma mark Message Notification Recived
- (void)messageReceived:(NSNotification*)textMessage
{
    NSLog(@"message received!");
    XMPPMessage *message = textMessage.object;
    NSString *adressString = [NSString stringWithFormat:@"%@",[message fromStr]];
    NSString *newStr = [adressString substringWithRange:NSMakeRange(1, [adressString length]-1)];
    NSString *facebookID = [NSString stringWithFormat:@"%@",[[newStr componentsSeparatedByString:@"@"] objectAtIndex:0]];
   
    BOOL isFriendInThisChat = NO;
    if (self.quickSendContacts.count) {//CHECK FOR QUICK SEND
        NSMutableArray *facebookIds = [NSMutableArray array];
        for (Conversation *conv in self.quickSendContacts)
            [facebookIds addObject:conv.facebookId];
        isFriendInThisChat = [facebookIds containsObject:facebookID];
    }else{//CHECK FOR PERSONAL CHAT
        isFriendInThisChat = [facebookID isEqualToString:self.conversation.facebookId];
    }
    
    // if message is not empty and sender is same with our _facebookID.
    if([message isChatMessageWithBody] && isFriendInThisChat/*([facebookID isEqualToString:self.conversation.facebookId])*/)
    {
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        Message *msg = [Message MR_createInContext:localContext];
        msg.text = [NSString stringWithFormat:@"%@",[[message elementForName:@"body"] stringValue]];
        msg.sentDate = [NSDate date];
        
        // message did come, this will be on left
        msg.messageStatus = @(YES);
        [self.conversation addMessagesObject:msg];
        
        if (self.quickSendContacts.count) {
            //DO NOT SHOW INCOMING MSG IN QUICK SEND
        }else{
            [self.messages addObject:msg];
        }
        
        [localContext MR_saveToPersistentStoreAndWait];
        
        if (!self.quickSendContacts.count) {//SHOW INCOMING MESSAGE ONLY FOR PERSONAL & GROUP CHAT
            [self finishSend];
        }
        [JSMessageSoundEffect playMessageReceivedSound];
    }
    else if([message isChatMessageWithBody] && !isFriendInThisChat/*(![facebookID isEqualToString:self.conversation.facebookId])*/)
    {
        // here message come fome another friend
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"facebookId = %@", facebookID];
        Conversation *conversation = [Conversation MR_findFirstWithPredicate:predicate inContext:localContext];
        
        Message *msg = [Message MR_createInContext:localContext];
        msg.text = [NSString stringWithFormat:@"%@",[[message elementForName:@"body"] stringValue]];
        msg.sentDate = [NSDate date];
        
        // message did come, this will be on left
        msg.messageStatus = @(YES);
        
        // increase badge number.
        int badgeNumber = [conversation.badgeNumber intValue];
        badgeNumber ++;
        conversation.badgeNumber = [NSNumber numberWithInt:badgeNumber];
        [conversation addMessagesObject:msg];
        [localContext MR_saveToPersistentStoreAndWait];
        
        
    }
}


#pragma mark - Messages view delegate
- (void)sendPressed:(UIButton *)sender withText:(NSString *)text
{
   // NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
   
    /* Save to Core data & FB database*/
    if (self.quickSendContacts.count) {//QUICK SEND
        Message *msg = nil;
        for (Conversation *conv in self.quickSendContacts){
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            msg = [Message MR_createInContext:localContext];
            msg.text = text;
            msg.sentDate = [NSDate date];
            msg.messageStatus = @(NO); // message did not come, this will be on right ----> Outgoing msg

            [conv addMessagesObject:msg];
            [[[FCAPIController sharedInstance] chatRequestManager] sendMessageToFacebook:text
                                                                    withFriendFacebookID:conv.facebookId];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        [self.messages addObject:msg];//datasource array

    }else{//PERSONAL CHAT
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];

        Message *msg = [Message MR_createInContext:localContext];
        msg.text = text;
        NSLog(@"msg.text=%@",msg.text);
        msg.sentDate = [NSDate date];
        msg.messageStatus = @(NO); // message did not come, this will be on right ----> Outgoing msg

        [self.conversation addMessagesObject:msg];
        
        [[[FCAPIController sharedInstance] chatRequestManager] sendMessageToFacebook:text
                                                            withFriendFacebookID:self.conversation.facebookId];
        [localContext MR_saveToPersistentStoreAndWait];
        [self.messages addObject:msg];//datasource array

    }
    
    
    [JSMessageSoundEffect playMessageSentSound];
    [self finishSend];
}

- (void)attachmentButtonPressed:(UIButton *)sender{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Gallery", @"Video", @"Contact", @"Location", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];

}
#pragma mark - UIActionSheetDelegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            //Camera
            break;
        case 1:
            //Gallery
            break;
        case 2:
            //Video
            break;
        case 3:
            //Contact
            break;
        case 4:
            //Location
            break;
    } 
}


- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.messages objectAtIndex:indexPath.row];
    if ([message.messageStatus boolValue]) {
        return JSBubbleMessageTypeIncoming;
    }else {
        return JSBubbleMessageTypeOutgoing;
    }
}

- (JSBubbleMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return JSBubbleMessageStyleDefault;//JSBubbleMessageStyleSquare;
}

- (JSMessagesViewTimestampPolicy)timestampPolicy
{
    return JSMessagesViewTimestampPolicyAll;
}

- (JSMessagesViewAvatarPolicy)avatarPolicy
{
    return JSMessagesViewAvatarPolicyBoth;
}

- (JSAvatarStyle)avatarStyle
{
    return JSAvatarStyleCircle;
}


#pragma mark - Messages view data source
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.messages objectAtIndex:indexPath.row];
    
    return [NSString stringWithFormat:@"%@\n%@",message.text,[NSDateFormatter localizedStringFromDate:message.sentDate dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.messages objectAtIndex:indexPath.row];
    return message.sentDate;
}

- (UIImage *)avatarImageForIncomingMessage
{
    return self.senderImage;
}

- (UIImage *)avatarImageForOutgoingMessage
{
    return self.reciverImage;
}

@end
