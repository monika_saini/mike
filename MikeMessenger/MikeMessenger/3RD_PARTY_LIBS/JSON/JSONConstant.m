//
//  JSONConstant.m
//  SmartZip
//
//  Created by Sagar Jadhav on 10/22/13.
//  Copyright (c) 2013 Rapidera. All rights reserved.
//

#import "JSONConstant.h"

@implementation JSONConstant

+(NSString *)getAPIKey
{
    NSString *strAPIKey;
    //strAPIKey = @"r4p1d3rasm4rtz33iOp";
    //strAPIKey = @"abcdef123456";
    strAPIKey = @"Sm4rtZ1p1ooSJ4n20L4";
    return strAPIKey;
}
+(NSString *)getBaseURL
{
    NSString *strBaseURL;
    //strBaseURL = @"http://release.smartzip.com/s/sz/mws/";
  strBaseURL = @"http://stage.smartzip.com/s/sz/mws/";
  // strBaseURL = @"https://mobileapi.smartzip.com/s/sz/mws/";
    return strBaseURL;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
