//
//  JSONConstant.h
//  SmartZip
//
//  Created by Sagar Jadhav on 10/22/13.
//  Copyright (c) 2013 Rapidera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONConstant : NSObject
+(NSString *)getAPIKey;
+(NSString *)getBaseURL;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
