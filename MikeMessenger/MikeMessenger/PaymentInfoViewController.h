//
//  PaymentInfoViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import <UIKit/UIKit.h>


@interface PaymentInfoViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    
    
    __weak IBOutlet UITextField *txtCardName;
    
    __weak IBOutlet UITextField *txtCardNumber;
    
    __weak IBOutlet UIBarButtonItem *btnBack;
    
    __weak IBOutlet UITextField *txtNameforCard;
    
    __weak IBOutlet UIBarButtonItem *btnSave;
    
    __weak IBOutlet UITextField *txtNikeName;
    
    __weak IBOutlet UIImageView *imgview;
    float lCurrentWidth;
    
}

@end
