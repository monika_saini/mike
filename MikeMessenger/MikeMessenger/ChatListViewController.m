//
//  ChatListViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/10/15.
//
//

#import "ChatListViewController.h"
#import "Conversation.h"
#import "FCConversationModel.h"
#import "UIImageView+WebCache.h"
#import "TDBadgedCell.h"
#import "XMPP.h"
#import "Message.h"
#import "NSString+Additions.h"
#import "FCChatDataStoreManager.h"
#import "FCRequestFacebookManager.h"
#import "FCAPIController.h"
#import "ChatMessageViewController.h"
#import "FCUser.h"
#import "ContactsListViewController.h"
#import "NewGroupViewController.h"
#import "QuickSendViewController.h"

@interface ChatListViewController ()
@property (nonatomic, strong) NSMutableArray *conversations;
@property (nonatomic, strong) NSMutableArray *searchResults;

@end

@implementation ChatListViewController
@synthesize editButton,arrContactsData;
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [[[FCAPIController sharedInstance] requestFacebookManager] requestGraphMeWithCompletion:^(NSDictionary *response, NSError *error){
        if (!error) {
            FCUser *currentUser = [[FCUser alloc] initWithDict:response];
            [[FCAPIController sharedInstance] setCurrentUser:currentUser];
            NSLog(@"This is the first step");
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    arrContactsData=[[NSMutableArray alloc]init];
    /*  UIBarButtonItem *driveModeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"car_icon.png"] style:(UIBarButtonItemStylePlain) target:self action:nil];
    self.tabBarController.navigationController.navigationBarHidden=NO;

  UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Edit"
                                  style:UIBarButtonItemStyleBordered
                                  target:self
                                  action:@selector(editButton:)];
    */
    //self.tabBarController.navigationItem.leftBarButtonItem=[NSArray arrayWithObject:editButton];
    
    
    
    UIButton *myButton = nil;
    myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myButton.bounds = CGRectMake(10,0,18,16);
    
    [myButton setImage:[UIImage imageNamed:@"car_icon.png"] forState:UIControlStateNormal];
    UIBarButtonItem *item = nil;
    item = [[UIBarButtonItem alloc] initWithCustomView:myButton];
    
   /* UIButton *btnEdit = nil;
    btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEdit.bounds = CGRectMake(10,0,18,16);
    
    //[myButton setImage:[UIImage imageNamed:@"car_icon.png"] forState:UIControlStateNormal];
    UIBarButtonItem *itemedit = nil;
    item = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
   */
    self.tabBarController.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:editButton,nil];
    
   // [myButton addTarget:self action:@selector(btnCar:) forControlEvents:UIControlEventTouchUpInside];


    self.tabBarController.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:self.composeButton,item,nil];
    //self.tabBarController.navigationItem.hidesBackButton=YES;
    self.tabBarController.navigationItem.title=@"Chats";

    
    //make datasource array of non-empty Conversations
   
    NSArray *allConversations=[[Conversation MR_findAll] mutableCopy];
    self.conversations = [NSMutableArray array];
    for (Conversation *conv in allConversations) {
        if ([conv.messages count]) {
            [self.conversations addObject:conv];
        }
    }
    [self.tView reloadData];
    /*if(allConversations.count==self.conversations.count){//no new chats
        [self.tView reloadData];
    
    }else{//new chats added
         [self.tView beginUpdates];
        self.conversations = [NSMutableArray array];
        for (Conversation *conv in allConversations) {
            if ([conv.messages count]) {
                [self.conversations addObject:conv];
            }
        }
    
        NSMutableArray *indexPaths = [NSMutableArray array];

        for (NSInteger row = 0; row < [self.conversations count]; row++) {
        
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            [indexPaths addObject:indexPath];
        }
        [self.tView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];

        [self.tView endUpdates];
    }*/
    
    //[self getCotact];
    
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    //ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    
    for(int i = 0; i < numberOfPeople; i++)
    {
        
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        
        NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        //   NSLog(@"Name:%@ %@", firstName, lastName);
        
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        NSString *phoneNumber;
        for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++)
        {
            phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
              NSLog(@"phone:%@", phoneNumber);
        }
     }
   }

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushChatUI"]) {
        NSIndexPath *indexPath = nil;
        Conversation *conversation = nil;
        
        if (self.searchDisplayController.active) {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            conversation = [self.searchResults objectAtIndex:indexPath.row];
        } else {
            indexPath = [self.tView indexPathForSelectedRow];
            conversation = [self.conversations objectAtIndex:indexPath.row];
        }
        
        ChatMessageViewController *chatMessageVC = (ChatMessageViewController *)[segue destinationViewController];
        chatMessageVC.conversation = conversation;
        chatMessageVC.isMultipleUserChat = NO;
       // NSIndexPath *indexpath = (NSIndexPath *)sender;
    }else if ([segue.identifier isEqualToString:@"presentPersonalChat"]){
        UINavigationController *navigationController = segue.destinationViewController;
        ContactsListViewController *contactListVC = [navigationController viewControllers][0];
        contactListVC.isGroupChat = NO;

    }else if ([segue.identifier isEqualToString:@"presentGroupChat" ]){
      /*  UINavigationController *navigationController = segue.destinationViewController;
        NewGroupViewController *newGroupVC = [navigationController viewControllers][0];*/
    }else if ([segue.identifier isEqualToString:@"presentQuickSend"]){
        UINavigationController *navigationController = segue.destinationViewController;
        QuickSendViewController *broadcastVC = [navigationController viewControllers][0];
        broadcastVC.isBroadcast = NO;
        
    }
}



#pragma mark Message Notification Recived
- (void)messageReceived:(NSNotification*)textMessage {
    
    XMPPMessage *message = textMessage.object;
    if([message isChatMessageWithBody]) {
        
        NSString *adressString = [NSString stringWithFormat:@"%@",[message fromStr]];
        NSString *newStr = [adressString substringWithRange:NSMakeRange(1, [adressString length]-1)];
        NSString *facebookID = [NSString stringWithFormat:@"%@",[[newStr componentsSeparatedByString:@"@"] objectAtIndex:0]];
        
        NSLog(@"FACEBOOK_ID:%@",facebookID);
        
        // Build the predicate to find the person sought
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"facebookId = %@", facebookID];
        Conversation *conversation = [Conversation MR_findFirstWithPredicate:predicate inContext:localContext];
        
        Message *msg = [Message MR_createInContext:localContext];
        msg.text = [NSString stringWithFormat:@"%@",[[message elementForName:@"body"] stringValue]];
        msg.sentDate = [NSDate date];
        
        // message did come, this will be on left
        msg.messageStatus = @(TRUE);
        
        // increase badge number.
        int badgeNumber = [conversation.badgeNumber intValue];
        badgeNumber++;
        conversation.badgeNumber = [NSNumber numberWithInt:badgeNumber];
        [conversation addMessagesObject:msg];
        [localContext MR_saveOnlySelfAndWait];
        
        [self.tView reloadRowsAtIndexPaths:[self.tView indexPathsForVisibleRows]
                              withRowAnimation:UITableViewRowAnimationNone];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.searchResults count];
        
    } else {
        return [self.conversations count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chatCell";
    TDBadgedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil )
    {
        cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryNone;

    Conversation *conversation = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        conversation = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        conversation = [self.conversations objectAtIndex:indexPath.row];
    }

    //Conversation *conversation = [self.conversations objectAtIndex:indexPath.row];
    if([conversation.badgeNumber intValue] != 0) {
        cell.badgeString = [NSString stringWithFormat:@"%@", conversation.badgeNumber];
        cell.badgeColor = [UIColor colorWithRed:0.197 green:0.592 blue:0.219 alpha:1.000];
        cell.badge.radius = 9;
    }else {
        cell.badgeString = @"";
        cell.badgeColor = [UIColor clearColor];
        cell.badge.radius = 0;
    }
    //NSLog(@"%@",conversation.lastMessage);
    NSMutableArray *allMessages = [conversation.messages allObjects].mutableCopy;
    NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"sentDate" ascending:YES];
    [allMessages sortUsingDescriptors:@[sortByDate]];
    NSLog(@"%@,%@",[[allMessages objectAtIndex:0] text], [[allMessages lastObject] text]);
    NSString *lastSentMessage=[NSString stringWithFormat:@"%@",[[allMessages lastObject] text]];

  
    
    NSString *url = [[NSString alloc]
                     initWithFormat:@"https://graph.facebook.com/%@/picture",conversation.facebookId];
    [cell.imageView setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType type){}];
    cell.imageView.layer.borderWidth = 1.0f;
    cell.imageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imageView.layer.masksToBounds = NO;
    cell.imageView.clipsToBounds = YES;
    cell.imageView.layer.cornerRadius = 25;
   
    [cell.textLabel setFont:[UIFont fontWithName:@"CenturyGothic-Bold" size:16.0]];
    cell.textLabel.text = conversation.facebookName;
    
    //if (indexPath.row) {
    cell.detailTextLabel.text = lastSentMessage;//@"Last message";//;

   /* }else
        cell.detailTextLabel.text = @"Love";//conversation.lastMessage;*/

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0f;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    [self performSegueWithIdentifier:@"pushChatUI" sender:indexPath];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //TODO: Delete conversation from core-data
    // Remove the row from data model
     NSString *facebookID = @"";//[NSString stringWithFormat:@"%@",[[newStr componentsSeparatedByString:@"@"] objectAtIndex:0]];
    Conversation *conv=nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        conv=[self.searchResults objectAtIndex:indexPath.row];
             [self.searchResults removeObjectAtIndex:indexPath.row];
        
    } else {
        conv=[self.conversations objectAtIndex:indexPath.row];

        [self.conversations removeObjectAtIndex:indexPath.row];
    }
    
    facebookID=[NSString stringWithFormat:@"%@",conv.facebookId];
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"facebookId = %@", facebookID];
    Conversation *conversation = [Conversation MR_findFirstWithPredicate:predicate inContext:localContext];
    [conversation MR_deleteEntity];
    [localContext MR_saveToPersistentStoreAndWait];

    
 
    // Request table view to reload
    [self.tView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    if (self.tView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

-(IBAction)editTableviewPessed:(id)sender{
    UIBarButtonItem *barButton = (UIBarButtonItem *)sender;
    if (barButton.tag==1) {//Done--->Edit,switch off
        self.tabBarController.navigationItem.leftBarButtonItem= self.editButton;
        [self.tView setEditing:NO animated:YES];

    }else{
        UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]initWithTitle:@"  Done" style:UIBarButtonItemStylePlain target:self action:@selector(editTableviewPessed:)];
        doneBarButton.tag=1;
        self.tabBarController.navigationItem.leftBarButtonItem= doneBarButton;
        [self.tView setEditing:YES animated:YES];
    }
}
-(void)getCotact
{
    
    CFErrorRef *error = nil;
    
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL)
    { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else
    { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
#ifdef DEBUG
        NSLog(@"Fetching contact info ----> ");
#endif
        
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        //NSMutableArray* items = [NSMutableArray arrayWithCapacity:nPeople];
        NSMutableArray *phoneNumbers=[[NSMutableArray alloc]init];
        
        for (int i = 0; i < nPeople; i++)
        {
           /* ContactsData *contacts = [ContactsData new];
            
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //get First Name and Last Name
            
            contacts.firstNames = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            contacts.lastNames =  (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            if (!contacts.firstNames) {
                contacts.firstNames = @"";
            }
            if (!contacts.lastNames) {
                contacts.lastNames = @"";
            }
            
            // get contacts picture, if pic doesn't exists, show standart one
            
            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
            contacts.image = [UIImage imageWithData:imgData];
            if (!contacts.image) {
                contacts.image = [UIImage imageNamed:@"NOIMG.png"];
            }*/
            //get Phone Numbers
            
             ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
           // NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++)
            {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [phoneNumbers addObject:phoneNumber];
                
               
                
            }
    
 
     
           // [contacts setNumbers:phoneNumbers];
            
            //get Contact email
            
            NSMutableArray *contactEmails = [NSMutableArray new];
            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
                
                [contactEmails addObject:contactEmail];
                // NSLog(@"All emails are:%@", contactEmails);
                
            }
            
            //[contacts setEmails:contactEmails];
            
            
            
           // [items addObject:contacts];
            
#ifdef DEBUG
            //NSLog(@"Person is: %@", contacts.firstNames);
            //NSLog(@"Phones are: %@", contacts.numbers);
            //NSLog(@"Email is:%@", contacts.emails);
#endif
            
            }
        
        NSLog(@"phonenumers=%@",phoneNumbers);
        
    } else {
#ifdef DEBUG
        NSLog(@"Cannot fetch Contacts :( ");        
#endif
       // return NO;
        
        
    }

    
/*
    // Use a general Core Foundation object.
    CFTypeRef generalCFObject = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    
    // Get the first name.
    if (generalCFObject) {
        [contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"firstName"];
        CFRelease(generalCFObject);
    }
    
    // Get the last name.
    generalCFObject = ABRecordCopyValue(person, kABPersonLastNameProperty);
    if (generalCFObject) {
        [contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"lastName"];
        CFRelease(generalCFObject);
    }

    */
   
}
- (IBAction)composePersonalChat:(id)sender{
    [self performSegueWithIdentifier:@"presentPersonalChat" sender:self];
}
/*
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    //invoke FB login sequencer
    __weak LoginViewController *self_ = self;
    if(indexPath.row==1){//FB
        [[[FCAPIController sharedInstance] authFacebookManager] authorize];
        [[FCAPIController sharedInstance] authFacebookManager].facebookAuthHandler = ^(NSNumber *sucess, NSError *error){
            if (!error) {
                [self_ runSequncerForIndexPath:indexPath];
            }
        };
        
    }
}
*/

 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
     return 30.0f;
 }
 



-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. The view for the header
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    // 2. Set a custom background color and a border
    headerView.backgroundColor = [UIColor whiteColor];
   /* headerView.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1.0].CGColor;
    headerView.layer.borderWidth = 1.0;*/
    
    UIButton *newGroupButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    [newGroupButton setBackgroundColor:[UIColor clearColor]];
    [newGroupButton.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:12.0]];
    [newGroupButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [newGroupButton addTarget:self action:@selector(createNewGroup) forControlEvents:UIControlEventTouchUpInside];
    [newGroupButton setTitle:@"New Group" forState:UIControlStateNormal];
    
    
    UIButton *quickSendButton = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width-80, 0, 80, 30)];
    [quickSendButton setBackgroundColor:[UIColor clearColor]];
    [quickSendButton.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:12.0]];
    [quickSendButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [quickSendButton addTarget:self action:@selector(createQuickSend) forControlEvents:UIControlEventTouchUpInside];
    [quickSendButton setTitle:@"Quick Send" forState:UIControlStateNormal];
    
    // 4. Add the label to the header view
    [headerView addSubview:newGroupButton];

    [headerView addSubview:quickSendButton];
    
    // 5. Finally return
    return headerView;
}
#pragma mark - Helper methods
-(void)createNewGroup{
    [self performSegueWithIdentifier:@"presentGroupChat" sender:self];
}

-(void)createQuickSend{
    [self performSegueWithIdentifier:@"presentQuickSend" sender:self];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
   // NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
   // NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"lastMessage contains[c] %@", searchText];

   // NSPredicate * resultPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1,predicate2,nil]];

    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
   
    self.searchResults = [[self.conversations filteredArrayUsingPredicate:resultPredicate] mutableCopy];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
