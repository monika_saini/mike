//
//  CustomBackView.h
//  MikeMessenger
//
//  Created by Monika on 2/17/15.
//
//

#import <UIKit/UIKit.h>

@interface CustomBackView : UIView
@property(nonatomic,strong)IBOutlet UIImageView *profilePicImageView;
@property(nonatomic,strong)IBOutlet UILabel *contactNameLabel, *statusLabel;
@property (nonatomic, strong) UIView *containerView;
+ (id)customView;
@end
