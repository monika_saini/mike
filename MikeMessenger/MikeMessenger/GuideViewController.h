//
//  GuideViewController.h
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import <UIKit/UIKit.h>

@interface GuideViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property NSUInteger pageIndex;
@property NSString *imageFile;
@end
