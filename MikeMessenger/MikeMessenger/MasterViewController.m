//
//  MasterViewController.m
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import "MasterViewController.h"
@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController
@synthesize btnSkip;
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Create the data model
    //_pageImages = @[@"page1.png", @"page2.png", @"page3.png", @"page4.png"];
    _pageImages = @[@"mike-page.png", @"mike-page.png", @"mike-page.png", @"mike-page.png"];

    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    GuideViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
  
   float lCurrentWidth = self.view.frame.size.width;
  //float  lCurrentHeight = self.view.frame.size.height;
    NSLog(@"lCurrentWidth=%f",lCurrentWidth);
    // Change the size of page view controller
    
    if (lCurrentWidth==320) {
        self.pageViewController.view.frame = CGRectMake(0, 0,320, self.view.frame.size.height - 70);
    }else
    {
       self.pageViewController.view.frame = CGRectMake(30, 0,335, self.view.frame.size.height - 70);
        btnSkip.translatesAutoresizingMaskIntoConstraints=YES;
        [btnSkip setFrame:CGRectMake(150, 606, 100, 32)];
        
    }
    
    //self.pageViewController.view.frame = CGRectMake(0,0,471, self.view.frame.size.height - 70);
    
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startWalkthrough:(id)sender {
    [self performSegueWithIdentifier:@"pushPasscodeVC" sender:self];
}

- (GuideViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageImages count] == 0) || (index >= [self.pageImages count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    GuideViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GuideViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((GuideViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((GuideViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageImages count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageImages count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushPasscodeVC"]) {
      
    }
}


@end
