//
//  PasscodeViewController.m
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import "PasscodeViewController.h"
#import "PhoneTableViewCell.h"
#import "LoginViewController.h"

#define kcountryPickerCellID @"countryPickerCell"
#define kphoneCellID @"phoneCell"


#define API_GETCODE @"http://hardcastlegis.co.in/Mike_Messenger/verifyUser/getCode.php"
#define API_VERIFYUSER @"http://hardcastlegis.co.in/Mike_Messenger/verifyUser/verifyUser.php"

#define API_AddresBook @"http://hardcastlegis.co.in/Mike_Messenger/userDetail/addressBook.php"

@interface PasscodeViewController ()
@property(nonatomic,strong)NSString *enteredPhone, *enteredPasscode;
@property(nonatomic,strong)NSUserDefaults *userDefaults;
@end

@implementation PasscodeViewController
@synthesize phoneView, passcodeView;
@synthesize countryCodeDict,arrcontact;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataClass_obj=[[DataClass alloc]init];
    arrcontact=[[NSMutableArray alloc]init];
    self.countryCodeDict=[NSDictionary dictionary];
    self.enteredPasscode=@"";
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    [self.userDefaults setObject:nil forKey:@"PhoneNumber"];
    [self.userDefaults synchronize];

    if ([self.userDefaults objectForKey:@"PhoneNumber"]!=nil) {//sms sent by server, but not entered by user...so show passcode screen
        self.phoneView.hidden = YES;
        self.passcodeView.hidden = NO;
        self.topLabel.text = @"Enter Passcode";
        self.nextButton.enabled = YES;
    }else{//(first time) || (failure in sending sms by server, user want to retry)
        self.phoneView.hidden = NO;
        self.passcodeView.hidden = YES;
        self.topLabel.text = @"Your Phone";
        self.nextButton.enabled = NO;
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [tapGestureRecognizer setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [tapGestureRecognizer setCancelsTouchesInView:NO];
    [txtfield1 addGestureRecognizer:tapGestureRecognizer1];
  
}
- (IBAction)btnBack_Click:(id)sender {
    [txtfield1 resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
    self.title=@"Verification";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
            //Do stuff here...
    //[txtfield1 becomeFirstResponder];
    //chkkeboardVal=1;
        [self.view endEditing:YES];
  
}

- (BOOL) validPhone:(NSString*) phoneString {
    if (phoneString.length) {
        NSError *error = NULL;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
        
        NSRange inputRange = NSMakeRange(0, [phoneString length]);
        NSArray *matches = [detector matchesInString:phoneString options:0 range:inputRange];
        
        // no match at all
        if ([matches count] == 0) {
            return NO;
        }
        
        // found match but we need to check if it matched the whole string
        NSTextCheckingResult *result = (NSTextCheckingResult *)[matches objectAtIndex:0];
        
        if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
            // it matched the whole string
            return YES;
        }
        else {
            // it only matched partial string
            return NO;
        }
    }else{
    
        return NO;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"presentISDList"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        ISDCodeTableViewController *ISDCodeTVC = [navigationController viewControllers][0];
        ISDCodeTVC.delegate = self;
        
    }
   
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
     [activity stopAnimating];
    if (alertView.tag==101 && buttonIndex==0) {
        
        CFErrorRef *error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        //ABAddressBookRef addressBook = ABAddressBookCreate();
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
        
        for(int i = 0; i < numberOfPeople; i++)
        {
            
            ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
            
            /*NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
            NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
            //   NSLog(@"Name:%@ %@", firstName, lastName);
            */
            
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
           
            NSString *phoneNumber;
            for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++)
            {
                phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                //NSLog(@"phone:%@", phoneNumber);
            
                [arrcontact addObject:phoneNumber];
                
              /*
                if (i==0) {
                   
                    string1 = phoneNumber;
                    //NSString *string2 = @" a test.";
                   // string1 = [string1 stringByAppendingString:string2];
                }else{
string1=[string1 stringByAppendingString:[NSString stringWithFormat:@",%@",phoneNumber]];
                }*/
            }
            
           
        }
                
        //NSLog(@"arrconatc=%@",arrcontact);
           
                [self callWebservice:arrcontact];

         [self performSegueWithIdentifier:@"pushLogin" sender:self];
    }
              
}

-(void)callWebservice:(NSMutableArray *)arraddrContact
{
     NSString *newStrphone=@"";
    NSLog(@"arrconatc=%@",arraddrContact);
    for (int i=0; i<[arraddrContact count]; i++)
    {
       
        NSString *phone=[arraddrContact objectAtIndex:i];
        if (i==arraddrContact.count-1) {
           newStrphone=[newStrphone stringByAppendingString:[NSString stringWithFormat:@"%@",phone]];
            
        }else
        {
       newStrphone=[newStrphone stringByAppendingString:[NSString stringWithFormat:@"%@,",phone]];
        }
    }
    NSLog(@"newphone=%@",newStrphone);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:API_AddresBook];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
   
    
    
    NSLog(@"params=%@",newStrphone);
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[newStrphone dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Response:%@ %@\n", response, error);
        if(error == nil)
        {
            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            
            NSLog(@"Data = %@",text);
        }else{
            
        }
      }];
    [dataTask resume];
    
}
#pragma mark - IBAction methods
-(IBAction)nextPressed:(UIButton *)sender{//bring passcode subview to front, hide phone subview
    
   /* PhoneTableViewCell *cell = (PhoneTableViewCell *)[self.tableView cellForRowAtIndexPath:0];
    [cell.phoneTextField resignFirstResponder];

   UITableViewCell *cell = [[self tableView] cellForRowAtIndexPath:0];
    if(cell == nil) {
        NSLog(@"Cell is nil"); // Cell is always nil
    } else {
UITextField *textField =(UITextField *)[cell viewWithTag:1];
        NSLog(@"Value is %@", textField.text);
    }*/
    
    [activity startAnimating];
    [self.view endEditing:YES];
    
    if (![self.passcodeView isHidden]) {//********REQUEST FOR CODE-VERIFICATION********
        if (self.enteredPasscode.length == 4) {//valid 4-digit passcode
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:API_VERIFYUSER];
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            NSString *params = [NSString stringWithFormat:@"MOBILE_CODE=%@&MOBILE_NUM=%@",self.enteredPasscode,self.enteredPhone];
           
                NSLog(@"params=%@",params);
            
            [urlRequest setHTTPMethod:@"POST"];
            [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSLog(@"Response:%@ %@\n", response, error);
                if(error == nil)
                {
                    NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                    
                     NSLog(@"Data = %@",text);
                    
                    if([text rangeOfString:@"SUCCESS"].location!=NSNotFound){//verified user
                        [self.userDefaults setObject:self.enteredPasscode forKey:@"SMS_Verified"];
                        [self.userDefaults synchronize];

                        [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Congratulations, you are a verified Mike's user now." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                        [self performSegueWithIdentifier:@"pushLogin" sender:self];
                        
                    }else{//verification failed---> wrong passcode
                        [activity stopAnimating];
                   UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Failure" message:@"OOPS! Mike verification for your number failed. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert setDelegate:self];
                        [alert setTag:101];
                        [alert show];
                        
                    }
                }else{
                    
                  
                    [activity stopAnimating];
                    [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"OOPS! Mike verification for your number failed. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];//error: timeout
                    
                }
            }];
            
            [dataTask resume];
        }else{// not a 4-digit passcode
        
            [activity stopAnimating];
             [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a valid 4-digit passcode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        }
    }else{//********REQUEST FOR SMS********
        if ([self validPhone:self.enteredPhone]) {
            
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:API_GETCODE];
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            NSString *params = [NSString stringWithFormat:@"COUNTRY=%@&MOBILE_NUM=%@",[self.countryCodeDict objectForKey:@"country"],self.enteredPhone/*[NSString stringWithFormat:@"%@%@",[self.countryCodeDict objectForKey:@"code"],self.enteredPhone]*/];
            NSLog(@"params=%@",params);
            
            [urlRequest setHTTPMethod:@"POST"];
            [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSLog(@"Response:%@ %@\n", response, error);
                if(error == nil)
                {
                    NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                    NSLog(@"Data = %@",text);
                    
                    if([text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@":"]].location!=NSNotFound){//sms received successfully from gateway
                    
    [self.userDefaults setObject:self.enteredPhone/*[NSString stringWithFormat:@"%@%@",[self.countryCodeDict objectForKey:@"code"],self.enteredPhone]*/ forKey:@"PhoneNumber"];
                        
                        //Load Passcode UI
                        self.phoneView.hidden = YES;
                        self.passcodeView.hidden = NO;
                        self.topLabel.text = @"Enter Passcode";
                      [activity stopAnimating];  
                    }else
                    {
                         [activity stopAnimating];
                        [self.userDefaults setObject:nil forKey:@"PhoneNumber"];
                        [activity stopAnimating];
                        [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"OOPS! Mike verification for your number failed. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    }
                    
                    [self.userDefaults synchronize];
                
                
                }else{
                [activity stopAnimating];
               [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"OOPS! Mike verification for your number failed. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                }
                
                
            } ];
           
            [dataTask resume];
            
        }else{
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a valid phone number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

#pragma mark - Textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{

}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag==1 /*|| textField.tag==2 || textField.tag==3 || textField.tag==4*/) {
        
        /*if (chkkeboardVal==1) {
            [textField becomeFirstResponder];
        }else{
            [textField resignFirstResponder];
        }*/
         self.enteredPasscode = textField.text;
        
    }else{
        self.enteredPhone=[NSString stringWithFormat:@"%@",textField.text];
        [self checkForFilledForm];
    }
}
/*
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag==1 || textField.tag==2 || textField.tag==3 || textField.tag==4) {
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSUInteger length = [currentString length];
        if (length > 1) {//single digit per textfield validation
            return NO;
        }else{
            //autofocus to next textfield logic
            
            textField.text=currentString;
           NSInteger nextTag = textField.tag + 1;// Try to find next responder
            UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
            if (nextResponder) {
                // Found next responder, so set it.
                [nextResponder becomeFirstResponder];
            } else {
                // Not found, so remove keyboard.
                [textField resignFirstResponder];
            }

            return NO;
        }
    }
    return YES;
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows.
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell= [self.tableView dequeueReusableCellWithIdentifier:kcountryPickerCellID];
    
    if (indexPath.row == 0){//Country cell
        
        UITableViewCell *cell0 = [self.tableView dequeueReusableCellWithIdentifier:kcountryPickerCellID];
        if (cell0 == nil) {
            cell0 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kcountryPickerCellID];
            [cell0 setAccessoryType:(UITableViewCellAccessoryDisclosureIndicator)];
        }
        cell0.textLabel.text=@"Country";
        
        if ([self.countryCodeDict objectForKey:@"country"]!=nil) {
            NSString *currentCountry = [NSString stringWithFormat:@"%@",[self.countryCodeDict objectForKey:@"country"]];
            cell0.detailTextLabel.text=currentCountry;

        }else
            cell0.detailTextLabel.text=@"";
        CALayer *upperBorder = [CALayer layer];
        upperBorder.backgroundColor = [[UIColor lightGrayColor] CGColor];
        upperBorder.frame = CGRectMake(15, 0, 320, 1.0f);
        [cell0.layer addSublayer:upperBorder];
        return cell0;

        
    }else {//Phone cell
        PhoneTableViewCell *cell1 = [self.tableView dequeueReusableCellWithIdentifier:kphoneCellID];
        if (cell1 == nil) {
            cell1 = [[PhoneTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kphoneCellID];
        }
        if ([self.countryCodeDict objectForKey:@"country"]==nil) {
            cell1.ISDLabel.text = @"";//@"+ISD";
            cell1.phoneTextField.text = @"";
            //[cell1.phoneTextField setTag:1];
        }else{
            cell1.ISDLabel.text = [NSString stringWithFormat:@"+%@",[self.countryCodeDict objectForKey:@"code"]];
           
            
           // cell.phoneTextField.text = @"";
        }
        return cell1;

    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  // chkkeboardVal=0;
    if (indexPath.row == 0) {//Country cell
        [self performSegueWithIdentifier:@"presentISDList" sender:self];
    }else{
        PhoneTableViewCell *cell = (PhoneTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell.phoneTextField becomeFirstResponder];
      
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){//present country-code list
        [self performSegueWithIdentifier:@"presentISDList" sender:self];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
// custom view for footer. will be adjusted to default or specified footer height
   
    UILabel *instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [instructionsLabel setBackgroundColor:[UIColor whiteColor]];
    
    [instructionsLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:14.0]];
        
    [instructionsLabel setTextColor:[UIColor grayColor]];
    [instructionsLabel setTextAlignment:(NSTextAlignmentCenter)];
    [instructionsLabel setNumberOfLines:0];
    [instructionsLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [instructionsLabel setText:@"Confirm your country code and enter your phone number."];
    
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor lightGrayColor] CGColor];
    upperBorder.frame = CGRectMake(15, 0, CGRectGetWidth(instructionsLabel.frame)-15, 1.0f);
    [instructionsLabel.layer addSublayer:upperBorder];
    return instructionsLabel;
 
}

#pragma mark - ISDCodeTableViewControllerDelegate

- (void)ISDCodeTableViewControllerDidCancel:(ISDCodeTableViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)ISDCodeTableViewControllerDidSave:(ISDCodeTableViewController *)controller withDict:(NSDictionary *)selectedDict{
    self.countryCodeDict=selectedDict;
    
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self checkForFilledForm];
}

-( void)checkForFilledForm{

    NSLog(@"%d%lu",self.countryCodeDict.count,(unsigned long)self.enteredPhone.length);
    if ([self.countryCodeDict count] && self.enteredPhone.length) {
        self.nextButton.enabled = YES;
    }else
        self.nextButton.enabled = NO;
    
    
    
}

@end
