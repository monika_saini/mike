//
//  TaskViewController.m
//  MikeMessenger
//
//  Created by Dipak B on 2/16/15.
//
//

#import "TaskViewController.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@interface TaskViewController ()

@end

@implementation TaskViewController
@synthesize arrTask,arrchk,arrData,calenderView,arrCompletedTaskCount,arrdublicateTask,arrdumytask;
- (void)viewDidLoad
{
  
    [super viewDidLoad];
    arrdumytask=[[NSMutableArray alloc]init];
    [selectdateview setHidden:YES];
    self.eventStore = [[EKEventStore alloc]init];
    arrCompletedTaskCount=[[NSMutableArray alloc]init];
    [timeView setHidden:YES];
    [btnmor setHidden:YES];
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory      inDomains:NSUserDomainMask] lastObject]);
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
   path = [documentsDirectory stringByAppendingPathComponent:@"tasklist.plist"]; //3
    
  [btnmor.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    [btnToday.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];

    [btnTomorrow.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    [btnSelectDate.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    
    [btnMorning.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    
    [btnPickerDobe.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    [btnCancel.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    
    [btnAfternoon.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    [btnEvening.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    

    [btnnight.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    

    [btnSelectTime.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0f]];
    

    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"tasklist" ofType:@"plist"]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    

    
    arrTask=[[NSMutableArray alloc]initWithContentsOfFile:path];
    
    NSLog(@"arrTask=%@",arrTask);
   arrdublicateTask=[[NSMutableArray alloc]init];
   
    
    
    for (int j=0;j<[arrTask count];j++) {
        [arrdublicateTask addObject:@"1"];
    }
    
    c=0;
    arrchk=[[NSMutableArray alloc]init];
    arrData=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrTask.count;i++)
    {
        [arrchk addObject:@"1"];
        //[arrCompletedTaskCount addObject:@"Completed"];
    }
    
  [calenderView setHidden:YES];
    
    
    // [self initlializeCalender];
    // Do any additional setup after loading the view.
    
    
}
- (IBAction)btnMorning:(id)sender
{
    c=1;
    [selectdateview setHidden:YES];
    [timeView setHidden:NO];
     [calenderView setHidden:YES];
}


- (IBAction)btnDone_click:(id)sende
{
    if ([txtCreateTask.text isEqualToString:@""])
    {
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"Please Enter task name" message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [Alert show];
        
    }else{
        [btnRemindme setUserInteractionEnabled:YES];
        [arrTask addObject:txtCreateTask.text];
       
        [arrTask writeToFile:path atomically:YES];
        
        [arrchk addObject:@"1"];
         //[arrCompletedTaskCount addObject:@"completed"];
        [tblTask reloadData];
        txtCreateTask.text=nil;
    }
    
    
}


- (IBAction)btnTomorrow_Click:(id)sender
{
    int daysToAdd = 1;
    NSDate *now = [NSDate date];
   date = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
     
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
  Stringdate= [dateFormat stringFromDate:date];
    NSLog(@"theDate=%@",Stringdate);
    [btnRemindme setTitle:@"Tomorrow" forState:UIControlStateNormal];
    [calenderView setHidden:YES];
}

- (IBAction)btnSelectDate_Click:(id)sender {
    c=2;
    t=1;
    [calenderView setHidden:YES];
    [selectdateview setHidden:NO];
    datePicker.datePickerMode=UIDatePickerModeDate;
    
}
- (IBAction)btnToday_Click:(id)sender
{
    date=[NSDate date];
   
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
   Stringdate = [dateFormat stringFromDate:date];
    NSLog(@"theDate=%@",Stringdate);
    [btnRemindme setTitle:@"Today" forState:UIControlStateNormal];
    [calenderView setHidden:YES];
}

- (IBAction)btnMorning_Click:(id)sender
{
    Strtime=@"03:30";
    [btnmor setTitle:@"Morning" forState:UIControlStateNormal];
    [timeView setHidden:YES];
    [self setEvent];
}

- (IBAction)btnafternoon_Click:(id)sender {
    
    Strtime=@"07:30";
    [btnmor setTitle:@"Afternoon" forState:UIControlStateNormal];
    [timeView setHidden:YES];
    [self setEvent];
}
- (IBAction)btnEvening_Click:(id)sender {
    Strtime=@"11:30";
    [btnmor setTitle:@"Evening" forState:UIControlStateNormal];
    [timeView setHidden:YES];
    [self setEvent];
}
- (IBAction)btnNightClick:(id)sender {
    Strtime=@"14:30";
    [btnmor setTitle:@"Night" forState:UIControlStateNormal];
    [timeView setHidden:YES];
    [self setEvent];
}
- (IBAction)btnPickerDone_Click:(id)sender {
    c=1;
    if (t==1)
    {
        NSDate *pickerDate = [datePicker date];
        NSLog(@"selected date=%@",pickerDate);
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        Stringdate= [dateFormat stringFromDate:pickerDate];
        NSLog(@"theDate=%@",Stringdate);
        
        unsigned units = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:units fromDate:pickerDate];
        
        NSInteger year = [components year];
        NSInteger month = [components month];
        NSInteger day = [components day];
//int monthNumber = 11;   //November
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
      
        
        NSString *finaldate=[NSString stringWithFormat:@"%ld %@",(long)day,monthName];
       
[btnRemindme setTitle:finaldate forState:UIControlStateNormal];
       
    }else{
       
        //NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
       // [outputFormatter setDateFormat:@"HH:mm"];//24hr time format
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       // [dateFormatter setDateFormat:@"hh:mm a"];
        [dateFormatter setDateFormat:@"HH:mm"];
     // NSString *timeString = [dateFormatter stringFromDate:datePicker.date];
      Strtime=[dateFormatter stringFromDate:datePicker.date];
     
         NSLog(@"Strtime=%@",Strtime);
   
       [btnmor setTitle:Strtime forState:UIControlStateNormal];
        [self setEvent];
    }
   
    
    [selectdateview setHidden:YES];
    
    
}
- (IBAction)btnPickerCancel_Click:(id)sender {
    c=1;
    [selectdateview setHidden:YES];
}

- (IBAction)btnSelectTime_Click:(id)sender {
   // [btnmor setTitle:@"Morning" forState:UIControlStateNormal];
    //c=3;
    t=2;
    [selectdateview setHidden:NO];
    datePicker.datePickerMode=UIDatePickerModeTime;
    [timeView setHidden:YES];
}

- (IBAction)btnRemindme_Click:(id)sender
{

 
    if (c==0)
    {
        [btnRemindme setTitle:@"Tomorrow" forState:UIControlStateNormal];
       
        [btnmor setHidden:NO];
                c=1;
    }else if(c==2){
        [timeView setHidden:YES];
        [calenderView setHidden:YES];
        c=1;
    }else
    {
        [timeView setHidden:YES];
        [calenderView setHidden:NO];
        [selectdateview setHidden:YES];
    }
    

    
    
    //[self performSegueWithIdentifier:@"goto-tomorrow" sender:self];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"goto-tomorrow"])
    {
        //TomorrowViewController *objTomo=segue.destinationViewController;
    }
}


- (IBAction)clearTask_Click:(id)sender
{
   
    cntarrtask=arrTask.count;
    val=arrTask.count;
    [timeView setHidden:YES];
    [selectdateview setHidden:YES];
    [calenderView setHidden:YES];
    clear=1;
    NSLog(@"arrCompletedTask=%@",arrCompletedTaskCount);
    
    NSLog(@"arrCompletedTaskCount=%d",arrCompletedTaskCount.count);
    
    for (cnt=0; cnt<arrCompletedTaskCount.count; cnt++)
    {
         NSLog(@"cntarrtask=%d",cntarrtask);
        if (arrCompletedTaskCount.count== cntarrtask)
        {
            [btnRemindme setUserInteractionEnabled:NO];
            if (arrTask.count!=0)
            {
                [arrTask removeObjectAtIndex:0];
                [arrchk removeObjectAtIndex:0];
            }
        }else{
            
            
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending:YES];
            NSArray *arrsorted = [arrCompletedTaskCount sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
            
             NSLog(@"arrsorted=%@",arrsorted);
            
           [btnRemindme setUserInteractionEnabled:YES];
            NSLog(@"value=%d",[[arrsorted objectAtIndex:cnt]integerValue]);
            
            [arrTask replaceObjectAtIndex:[[arrsorted objectAtIndex:cnt]integerValue] withObject:@"0"];
   
        }
    

            NSLog(@"arrtask=%@",arrTask);
            
            NSLog(@"arrchk=%@",arrchk);
    }
    
    //arrdumytask=arrTask;
    
    if ([arrTask containsObject:@"0"]) {
        
        [arrTask removeObject:@"0"];
        [arrchk removeObject:@"0"];
    }
   
    
    
   
    NSLog(@"arrtask=%@",arrTask);
    
    NSLog(@"arrchk=%@",arrchk);
    
    [arrTask writeToFile:path atomically:YES];
    
    
  
    [arrCompletedTaskCount removeAllObjects];
   // [arrdublicateTask removeAllObjects];
    cntarrtask=0;
    val=0;
    [tblTask reloadData];
    
}

-(void)setEvent
{
    NSString *dateString1=[NSString stringWithFormat:@"%@T%@%@",Stringdate,Strtime,@":00Z"];
    NSLog(@"dateString=%@",dateString1);
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    //NSDate *date12 = [dateFormatter1 dateFromString:@"2015-03-05T08:00:47Z"];
    
    NSDate *date12 = [dateFormatter1 dateFromString:dateString1];
    
    NSDate *newDate = [date12 dateByAddingTimeInterval:-60*330];
    NSLog(@"newDate=%@",newDate);
     NSLog(@"date12=%@",date12);
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone localTimeZone]];

    NSString *stringFromDate = [formatter stringFromDate:newDate];
   
    NSLog(@"stringFromDate=%@",stringFromDate);
    
    bookingDate = newDate;

   
    //NSString * NoteDetails = [NSString stringWithFormat:@"%@, %@ %@",self.cb.address,self.cb.suburb,self.cb.postalcode];
    
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             
         /*
              //EVENT ONE DAY BEFORE
              EKEvent *event1  = [EKEvent eventWithEventStore:self.eventStore];
              NSDateComponents *tomorrowDateComponents = [[NSDateComponents alloc] init];
              tomorrowDateComponents.day = 0;
              event1.title =@"Whizz Cleaner";
              //event1.notes = NoteDetails;
            event1.notes = [NSString stringWithFormat:@"Don’t forget, you have scheduled clean tomorrow at %@",stringFromDate];
              event1.allDay=NO;
             
             NSDate *ago = [bookingDate dateByAddingTimeInterval:-(1440*60)];
             
             //[event addAlarm:[EKAlarm alarmWithAbsoluteDate:ago]];
              
              event1.startDate=ago;
              event1.endDate=bookingDate;
              
              [event1 setCalendar:[self.eventStore defaultCalendarForNewEvents]];
              
              NSError *err1;
              BOOL success1 = [self.eventStore saveEvent:event1 span:EKSpanThisEvent error:&err1];
              
              if(success1)
              {
              NSLog(@"Event 1=  %@",event1.eventIdentifier);
              //eventId1 = event1.eventIdentifier;
              }
              else
              {
             // eventId1 = @"";
              }
            
             */
             //EVENT ON Exact Date And Time + Duration
             
EKEvent *event2  = [EKEvent eventWithEventStore:self.eventStore];
    NSDateComponents *tomorrowDateComponents = [[NSDateComponents alloc] init];
             tomorrowDateComponents.day = 0;
             event2.title =@"Mike Task list ";
             event2.notes = [NSString stringWithFormat:@"Mike task list"];
             event2.allDay=NO;
             NSDate *end = [bookingDate dateByAddingTimeInterval:(60*60)];
             
    //[event addAlarm:[EKAlarm alarmWithAbsoluteDate:ago]];
             
             event2.startDate=bookingDate;
             event2.endDate=end;
             
             [event2 setCalendar:[self.eventStore defaultCalendarForNewEvents]];
             
             NSError *err2;
             BOOL success2 = [self.eventStore saveEvent:event2 span:EKSpanThisEvent error:&err2];
             
             if(success2)
             {
                 NSLog(@"Event 2 =  %@",event2.eventIdentifier);
                // eventId2 = event2.eventIdentifier;
             }
             else
             {
                 //eventId2 = @"";
             }
             
             
             
         }
         else
         {
             NSLog(@"NoPermission to access the calendar");
         }
         //[self createRemainder:cb];
     }];
}

-(void)viewWillAppear:(BOOL)animated
{
  
    if(arrTask.count==0)
    {
        [btnRemindme setUserInteractionEnabled:NO];
    }else{
        [btnRemindme setUserInteractionEnabled:YES];
    }
    //[self.parentViewController.navigationController setNavigationBarHidden:YES];
  tblTask.rowHeight = UITableViewAutomaticDimension;
   tblTask.estimatedRowHeight = 44.0;
    
  /*  UIBarButtonItem *btnShare1 = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Share"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(share:)];
   
    
    
    self.tabBarController.navigationItem.rightBarButtonItems=[NSArray arrayWithObject:btnShare1];
    */
    self.tabBarController.navigationItem.leftBarButtonItem=nil;
    self.tabBarController.navigationItem.hidesBackButton=YES;
    
    UIButton *myButton = nil;
    myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myButton.bounds = CGRectMake(30,0,60,30);
    
    [myButton setTitle:@"Share" forState:UIControlStateNormal];
    // setup myButton's images, etc.
    
    UIBarButtonItem *item = nil;
item = [[UIBarButtonItem alloc] initWithCustomView:myButton];
   
    [myButton addTarget:self action:@selector(btnShare:) forControlEvents:UIControlEventTouchUpInside];
    self.tabBarController.navigationItem.rightBarButtonItems=[NSArray arrayWithObject:item];
  
    self.tabBarController.navigationItem.title=@"Tasks";
    
     self.tabBarController.navigationItem.leftBarButtonItem=nil;
    
    self.navigationController.navigationItem.leftBarButtonItem=nil;
    
[btnDone.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
    [txtCreateTask setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
    
    [btnRemindme.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
   
    [btnClearTask.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
    

    
    //To make the border look very close to a UITextField
    [txtCreateTask.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [txtCreateTask.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    txtCreateTask.layer.cornerRadius = 9;
    txtCreateTask.clipsToBounds = YES;
    [txtCreateTask setDelegate:self];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtCreateTask resignFirstResponder];
    return YES;
}
-(void)btnShare:(UIButton *)sender
{
   [self openmailmessage];
}


-(void)openmailmessage
{
    UIActivityViewController *activityVC;
    //NSString *textToShare = @"I wants you to try SuperSpeller App.";
    //NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    // UIImage *image = [UIImage imageNamed:];
    //  NSArray *activityItems = @[textToShare];
    
    DMActivityInstagram* instagramShareActivity = [[DMActivityInstagram alloc] init];
    
    
    //instagramShareActivity.viewToDisplay = self.view;
    
    
    activityVC = [[UIActivityViewController alloc] initWithActivityItems:arrTask applicationActivities:@[instagramShareActivity]];
    
   NSArray *excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                       UIActivityTypePrint,
                                       UIActivityTypeCopyToPasteboard,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToTencentWeibo,
                                       UIActivityTypeAirDrop,
                                       UIActivityTypePostToFacebook,
                                       UIActivityTypePostToTwitter,
                                       ];
    
    [activityVC setExcludedActivityTypes:excludedActivityTypes];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
    
    [activityVC setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSString *ServiceMsg=nil;
         if([act isEqualToString:UIActivityTypeMail])
         {
             ServiceMsg=@"Mail sent";
             
         } else if([act isEqualToString:UIActivityTypeMessage]) {
             ServiceMsg=@"Message sent!";
         }
         
         if ( done )
         {
             UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
             [Alert show];
             
         }
     }];
    
 
}
-(void)share:(UIButton *)sender
{
    [self openmailmessage];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
   //noofTask=arrTask.count;
    return arrTask.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"taskCell";
CreateTaskCell *cell = [tblTask dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CreateTaskCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
   
    
    NSString *strchk=[arrchk objectAtIndex:indexPath.row];
    
    if (strchk.integerValue==1)
    {
      [cell.btnuncheck setImage:[UIImage imageNamed:@"tik_box"] forState:UIControlStateNormal];
        
        [cell.lblLine setHidden:YES];
        [cell.btnDelete setHidden:YES];
    }else
    {
    [cell.btnuncheck setImage:[UIImage imageNamed:@"tik_mark"] forState:UIControlStateNormal];
        [cell.lblLine setHidden:NO];
        [cell.btnDelete setHidden:NO];
        
    }
    
    
    
[cell.lblTaskName setText:[arrTask objectAtIndex:indexPath.row]];
    
 [cell.lblTaskName setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
    
    
    
    [cell.btnDelete setTag:indexPath.row];
    [cell.btnuncheck setTag:indexPath.row];
    
    [cell.btnuncheck addTarget:self action:@selector(btnuncheck_Click:) forControlEvents:UIControlEventTouchUpInside];
     
    [cell.btnDelete addTarget:self action:@selector(btnDelete_Click:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
  
      return cell;
}
-(void)btnDelete_Click:(UIButton *)sender
{
    [arrTask removeObjectAtIndex:sender.tag];
    
    [arrTask writeToFile:path atomically:YES];
    
    [arrchk removeObjectAtIndex:sender.tag];
    [tblTask reloadData];
    
}
-(void)btnuncheck_Click:(UIButton *)sender
{
    
   // UIButton *button = (UIButton*)sender;
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    //Type cast it to CustomCell
    CreateTaskCell *cell = (CreateTaskCell*)[tblTask cellForRowAtIndexPath:myIP];
  
   
    NSString *strVal=[arrchk objectAtIndex:myIP.row];
    
    if (strVal.integerValue==1)
    {
    [cell.btnuncheck setImage:[UIImage imageNamed:@"tik_mark"] forState:UIControlStateNormal];
        [arrchk replaceObjectAtIndex:myIP.row withObject:@"0"];
        [arrCompletedTaskCount addObject:[NSString stringWithFormat:@"%ld",(long)myIP.row]];
        
    }else
    {
      
        [cell.btnuncheck setImage:[UIImage imageNamed:@"tik_box"] forState:UIControlStateNormal];
        [arrchk replaceObjectAtIndex:myIP.row withObject:@"1"];
    }
    
    
    [tblTask reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
