//
//  PasscodeViewController.h
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import <UIKit/UIKit.h>
#import "ISDCodeTableViewController.h"
#import "DataClass.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface PasscodeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ISDCodeTableViewControllerDelegate,UIAlertViewDelegate,ABPeoplePickerNavigationControllerDelegate>
{
    __weak IBOutlet UIActivityIndicatorView *activity;
    DataClass *dataClass_obj;
    __weak IBOutlet UITextField *txtFeild4;
    __weak IBOutlet UITextField *txtField3;
    __weak IBOutlet UITextField *txtField2;
    __weak IBOutlet UITextField *txtfield1;
    NSInteger chkkeboardVal;
}
@property(strong,nonatomic)NSDictionary *countryCodeDict;
@property(nonatomic,strong)IBOutlet UIView *phoneView, *passcodeView;
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)IBOutlet UILabel *topLabel;
@property(nonatomic,strong)IBOutlet UIButton *nextButton;
@property(nonatomic,retain)NSMutableArray *arrcontact;
-(IBAction)nextPressed:(UIButton *)sender;

@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;
-(void)callWebservice:(NSMutableArray *)arraddrContact;
@end
