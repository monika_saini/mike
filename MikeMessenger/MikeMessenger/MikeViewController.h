//
//  MikeViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/19/15.
//
//

#import <UIKit/UIKit.h>

@interface MikeViewController : UIViewController<UITextFieldDelegate,UIPopoverPresentationControllerDelegate>
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
@property(nonatomic,strong)IBOutlet UITextField *msgTextfield;

@property(nonatomic,strong)IBOutlet UICollectionView *collectionView;
-(IBAction)sendPressed:(id)sender;
@end
