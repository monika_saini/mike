//
//  ChatSettingsViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import <UIKit/UIKit.h>
#import "notificationCell.h"


@interface ChatSettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblChatSettings;
@property(nonatomic,retain)NSMutableArray *arrData;

@end
