//
//  PaymentInfoViewController.m
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import "PaymentInfoViewController.h"

@interface PaymentInfoViewController ()

@end

@implementation PaymentInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     lCurrentWidth = self.view.frame.size.width;
    //float  lCurrentHeight = self.view.frame.size.height;
    NSLog(@"lCurrentWidth=%f",lCurrentWidth);
    // Change the size of page view controller
 
}
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBack_click:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Save"
                                  style:UIBarButtonItemStyleBordered
                                  target:self
                                  action:@selector(btnSave_Click:)];
    
    self.tabBarController.navigationItem.rightBarButtonItems=[NSArray arrayWithObject:save];
    
  // [self.parentViewController.navigationController setNavigationBarHidden:YES];
    //self.navigationController.navigationBar.topItem.title = @"Back";
    
     //
   /*  UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = item;
    
   [btnSave setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [UIFont fontWithName:@"CenturyGothic" size:18.0f], NSFontAttributeName,
                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                      nil]
                            forState:UIControlStateNormal];

    
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIFont fontWithName:@"CenturyGothic" size:18.0f], NSFontAttributeName,
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     nil]
                           forState:UIControlStateNormal];*/
   
  /*  NSShadow* shadow = [NSShadow new];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [UIColor whiteColor],
                                                            NSFontAttributeName: [UIFont fontWithName:@"CenturyGothic" size:18.0f],
                                                            NSShadowAttributeName:shadow
                                                            }];
    
*/
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
   
}

- (IBAction)btnCameraClick:(id)sender
{
    
UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
   
    //imgview.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.view.translatesAutoresizingMaskIntoConstraints=YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    if(lCurrentWidth==320)
    {
        self.view.frame = CGRectMake(0,-40,320,480);
        
    }else{
        self.view.frame = CGRectMake(0,-40,375,480);
    }
    
    [UIView commitAnimations];

       return YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   
    if (textField==txtCardName)
    {
    
        
        [txtCardName resignFirstResponder];
    }
    if (textField==txtCardNumber)
    {
        
        
        [txtCardNumber resignFirstResponder];
    }
    if (textField==txtNameforCard)
    {
        
        
        [txtNameforCard resignFirstResponder];
    }
    if (textField==txtNikeName)
    {
        [txtNikeName resignFirstResponder];
    }
    
    
    
    if (lCurrentWidth==320) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.25];
        self.view.frame = CGRectMake(0,0,320,568);
        [UIView commitAnimations];
        
  
        
    }else
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.25];
        self.view.frame = CGRectMake(0,0,400,667);
        [UIView commitAnimations];
        

    }
    
   
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
