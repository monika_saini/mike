//
//  ContactsListViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/16/15.
//
//
//TODO:handle selectedContacts for group-chat after search

#import "ContactsListViewController.h"
#import "UIImageView+WebCache.h"
#import "ChatMessageViewController.h"
#import "QuickSendViewController.h"
#import "NewGroupViewController.h"
#import "FCAPIController.h"
#import "FCRequestFacebookManager.h"
#import "FCChatDataStoreManager.h"
#import "Conversation.h"
#import "FBRequest.h"

@interface ContactsListViewController ()
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic) NSMutableArray *sectionsArray;
@property (nonatomic) UILocalizedIndexedCollation *collation;

//- (void)configureSections;
-(NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector;
@end

@implementation ContactsListViewController
@synthesize friendsList;
@synthesize isGroupChat;
@synthesize selectedContacts;

- (void)viewDidLoad {
    [super viewDidLoad];
   
       // Do any additional setup after loading the view.
    if(self.isGroupChat)
        self.selectedContacts=[NSMutableArray array];
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // [self setContactsArray:self.friendsList];
    [self setContactsArray];
    if(!self.isGroupChat)
        [self.navigationItem setRightBarButtonItem:nil];
    [self.tView reloadRowsAtIndexPaths:[self.tView indexPathsForVisibleRows]
                      withRowAnimation:UITableViewRowAnimationNone];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
      searchBar.showsCancelButton=NO;
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Search Clicked");
    [self searchTableList];
}


- (void)searchTableList {
    
    NSString *searchString = searchBar.text;
    
    filtered = [contactsInSection filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.Name CONTAINS[cd] %@",searchString]];
    
    NSLog(@"filtered= %@",filtered);
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushPersonalChat"]){
        ChatMessageViewController *chatUIVC = segue.destinationViewController;
        
        if (self.isGroupChat) {
            //TODO:Core-data for group-chat conversation
            chatUIVC.isMultipleUserChat = YES;

        }else{
            NSIndexPath *indexPath = nil;
            NSDictionary *friendDict = nil;
        
            if (self.searchDisplayController.active) {
                indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
                friendDict = [self.searchResults objectAtIndex:indexPath.row];
            }else {
                indexPath = [self.tView indexPathForSelectedRow];
                friendDict = self.sectionsArray [indexPath.section][indexPath.row];
            }
            
            // Build the predicate to find the person sought
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"facebookId = %@", [friendDict valueForKey:@"id"]];
            Conversation *conversation = [Conversation MR_findFirstWithPredicate:predicate inContext:localContext];
        
            chatUIVC.conversation = conversation;
            chatUIVC.isMultipleUserChat = NO;
        }
        
    }else if ([segue.identifier isEqualToString:@"presentBroadCast"]){
        UINavigationController *navigationController = segue.destinationViewController;
        QuickSendViewController *broadcastVC = [navigationController viewControllers][0];
        broadcastVC.isBroadcast = YES;
        
    }else if ([segue.identifier isEqualToString:@"presentGroupChat" ]){
       /* UINavigationController *navigationController = segue.destinationViewController;
        NewGroupViewController *newGroupVC = [navigationController viewControllers][0];*/
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 1;
        
    } else {

    // The number of sections is the same as the number of titles in the collation.
    return [[self.collation sectionTitles] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.searchResults count];
        
    } else {

    // The number of time zones in the section is the count of the array associated with the section in the sections array.
    contactsInSection = (self.sectionsArray)[section];
    
    return [contactsInSection count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ContactCell"];
    }
    NSDictionary *friendDict = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        friendDict = self.searchResults[indexPath.row];
        
    } else {
    
        // Get the contacts from the array associated with the section index in the sections array.
        NSArray *contactsInSection = (self.sectionsArray)[indexPath.section];
        friendDict = contactsInSection[indexPath.row];
    }
    if(self.isGroupChat && [selectedContacts containsObject:friendDict])
        [cell setAccessoryType:(UITableViewCellAccessoryCheckmark)];
    else
        [cell setAccessoryType:(UITableViewCellAccessoryNone)];
    
    NSString *url = [[NSString alloc]
                     initWithFormat:@"https://graph.facebook.com/%@/picture",[friendDict valueForKey:@"id"]];
    [cell.imageView setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType type){}];
    cell.imageView.layer.borderWidth = 1.0f;
    cell.imageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imageView.layer.masksToBounds = NO;
    cell.imageView.clipsToBounds = YES;
    cell.imageView.layer.cornerRadius = 25;
    
    // Configure the cell...
    [cell.textLabel setFont:[UIFont fontWithName:@"CenturyGothic-Bold" size:16.0]];
    //[cell.detailTextLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:14.0]];
    
    cell.textLabel.text = [friendDict valueForKey:@"name"];
    //cell.detailTextLabel.text = @"Status";
   
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0f;    
}

/*
 Section-related methods: Retrieve the section titles and section index titles from the collation.
 */

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
    return [self.collation sectionTitles][section];
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [self.collation sectionIndexTitles];
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    } else {
        return [self.collation sectionForSectionIndexTitleAtIndex:index];
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isGroupChat){//multi-select
        Conversation *conversation = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            conversation = self.searchResults[indexPath.row];
            
        } else {
            conversation = self.sectionsArray[indexPath.section][indexPath.row];
        }
        UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
            thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
            [selectedContacts addObject:conversation];
        }
        else
        {
            thisCell.accessoryType = UITableViewCellAccessoryNone;
            [selectedContacts removeObject:conversation];
        }
    
    }else
        [self performSegueWithIdentifier:@"pushPersonalChat" sender:self];
}


#pragma mark - IBActions
- (IBAction)cancel:(id)sender
{
    if(self.isGroupChat)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)nextPressed:(id)sender{
    [self performSegueWithIdentifier:@"pushPersonalChat" sender:self];
}


-(IBAction)createNewGroup{
    [self performSegueWithIdentifier:@"presentGroupChat" sender:self];
}

-(IBAction)createBroadcast{
    [self performSegueWithIdentifier:@"presentBroadCast" sender:self];
}

#pragma mark - Set the data array and configure the section data

/*
 Configuring the sections is a little fiddly, and it's not directly relevant to understanding how sectioned table views themselves work, so there's no need to work though these methods if you don't want to.
 */

- (void)setContactsArray{
    [[[FCAPIController sharedInstance] requestFacebookManager] requestGraphFriendsWithCompletion:^(NSArray *responseArray, NSError *error) {
        if (!error) {
            self.friendsList = [NSMutableArray arrayWithArray:responseArray];
            if (self.friendsList == nil) {//no friends
                self.sectionsArray = nil;
                [[[UIAlertView alloc] initWithTitle:@"OOPS" message:@"Please add some friends to chat." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
            else {//show friend-list in UI
                //[self configureSections];
                self.sectionsArray = [self partitionObjects:[self friendsList] collationStringSelector:@selector(self)].mutableCopy;
                [self.tView reloadData];
            }

        }else{//error in fetching FB friend list
         [[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    
}


/*
- (void)configureSections {
    
    // Get the current collation and keep a reference to it.
    self.collation = [UILocalizedIndexedCollation currentCollation];
    
    NSInteger index, sectionTitlesCount = [[self.collation sectionTitles] count];
    
    NSMutableArray *newSectionsArray = [[NSMutableArray alloc] initWithCapacity:sectionTitlesCount];
    
    // Set up the sections array: elements are mutable arrays that will contain the time zones for that section.
    for (index = 0; index < sectionTitlesCount; index++) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [newSectionsArray addObject:array];
    }
    
    // Segregate the time zones into the appropriate arrays.
    for (NSDictionary *friendDict in self.friendsList) {
        IDSetter *contactName = (IDSetter *)[NSString stringWithFormat:@"%@",[friendDict valueForKey:@"name"]];
        // Ask the collation which section number the time zone belongs in, based on its locale name.
        NSInteger sectionNumber = [self.collation sectionForObject:contactName collationStringSelector:@selector(copy)];//@selector(localeName)
       
        // Get the array for the section.
        NSMutableArray *sectionContacts = newSectionsArray[sectionNumber];
        
        //  Add the time zone to the section.
        //[sectionContacts addObject:contactName];
        [sectionContacts addObject:friendDict];

    }
    
    // Now that all the data's in place, each section array needs to be sorted.
    for (index = 0; index < sectionTitlesCount; index++) {
              NSMutableArray *contactsArrayForSection = newSectionsArray[index];
        
        // If the table view or its contents were editable, you would make a mutable copy here.
        NSArray *sortedContactsArrayForSection = [self.collation sortedArrayFromArray:contactsArrayForSection collationStringSelector:@selector(copy)];
        
        // Replace the existing array with the sorted array.
        newSectionsArray[index] = sortedContactsArrayForSection;
    }
    
    self.sectionsArray = newSectionsArray;
    [self.tView reloadData];
}
*/

-(NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector
{
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    self.collation = collation;
    NSInteger sectionCount = [[collation sectionTitles] count];
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    for (int i = 0; i < sectionCount; i++) {
        [unsortedSections addObject:[NSMutableArray array]];
    }
    
    for (id object in array) {
        NSInteger index = [collation sectionForObject:[object objectForKey:@"name"] collationStringSelector:selector];
        [[unsortedSections objectAtIndex:index] addObject:object];
    }
    
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    
    for (NSMutableArray *section in unsortedSections) {
        NSArray *sortedArray =[section sortedArrayUsingDescriptors:sortDescriptors]; //[section sortedArrayUsingDescriptors:sortDescriptors]collationStringSelector:selector]];
       // NSArray *sortedArray = [section sortedArrayFromArray:contactsArrayForSection collationStringSelector:selector];

        [sections addObject:sortedArray];
    }
    
    return sections;
}




-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
    // NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"lastMessage contains[c] %@", searchText];
    
    // NSPredicate * resultPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1,predicate2,nil]];
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    
    self.searchResults = [[self.friendsList filteredArrayUsingPredicate:resultPredicate] mutableCopy];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSLog(@"%@,%@",searchString,[[self.searchDisplayController.searchBar scopeButtonTitles]
                                 objectAtIndex:[self.searchDisplayController.searchBar
                                                selectedScopeButtonIndex]]);
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}
 

@end
