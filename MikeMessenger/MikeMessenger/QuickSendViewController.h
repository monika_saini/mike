//
//  QuickSendViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/19/15.
//
//

#import <UIKit/UIKit.h>

@interface QuickSendViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign) BOOL isBroadcast;
@property(nonatomic,strong)IBOutlet UITableView *tView;
@property (nonatomic, strong) NSMutableArray *selectedContacts;

-(IBAction)cancelPressed:(id)sender;
-(IBAction)createQuickSend;
@end
