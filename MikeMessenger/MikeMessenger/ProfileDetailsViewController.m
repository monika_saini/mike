//
//  ProfileDetailsViewController.m
//  MikeMessenger
//
//  Created by Dipak B on 2/17/15.
//
//

#import "ProfileDetailsViewController.h"

@interface ProfileDetailsViewController ()

@end

@implementation ProfileDetailsViewController
@synthesize data_Dict,arrDay,arrMonth,arrYear;
- (void)viewDidLoad {
    [super viewDidLoad];
    picker=1;
    //NSLog(@"Dict:%@",data_Dict);
    [picuiview setHidden:YES];
    arrDay=[[NSMutableArray alloc]initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31", nil];
    
   arrMonth=[[NSMutableArray alloc]initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    
    arrYear=[[NSMutableArray alloc]initWithObjects:@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2012", nil];
    
    
    dataClass_obj=[[DataClass alloc]init];
    [self webserviceData];
    
    /*
     *  This will enable horizontal scrolling.
     */
    //self.content1WidthConstraint.constant = 500;
    
    self.content1HeightConstraint.constant=600;
    
    /*
     *  This will enable vertical scrolling.
     */
    //self.content2HeightConstraint.constant = 200;
    lCurrentWidth = self.view.frame.size.width;
    //float  lCurrentHeight = self.view.frame.size.height;
    NSLog(@"lCurrentWidth=%f",lCurrentWidth);

    if(lCurrentWidth==320)
    {
        self.content1WidthConstraint.constant=320;
        
    }else{
        self.content1WidthConstraint.constant=375;
    }

 
}


// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (picker==1) {
        return arrDay.count;
    }else if (picker==2){
        return arrMonth.count;
    }else{
        return arrYear.count;
    }
    
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (picker==1) {
        return arrDay[row];
    }else if (picker==2){
        return arrMonth[row];
    }else{
        return arrYear[row];
    }
    
}


- (IBAction)btnSubmit_Click:(id)sender
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@" http://hardcastlegis.co.in/Mike_Messenger/userDetail/editUserDetail.php"]];
    [request setHTTPMethod:@"POST"];
    
NSString *userUpdate =[NSString stringWithFormat:@"FIRST_NAME=%@&LAST_NAME=%@&MOBILE_NUM=%@&OFFICE_NUM=%@&HOME_NUM=%@&PERSONAL_EMAIL=%@&OFFICIAL_EMAIL=%@&HOME_ADDRESS=%@&OFFICE_ADDRESS=%@&GENDER=%@&DOB=%@",txtFirstName.text,txtLastName.text,txtMobilt.text,txtOffMobile.text,txtHomeMobile.text,txtOfficeMail.text,txtPersonalMail.text,txtOffAddr.text,txtHomeAddr.text,txtMaleFemale.text,@"22-05-1985"];
    
    NSLog(@"userUpdate=%@",userUpdate);
    
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
   
    [request setHTTPBody:data1];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *resp,NSData *data,NSError *error)
     {
         NSString *txt = [[NSString alloc] initWithData:data encoding: NSASCIIStringEncoding];
         NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [txt dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingAllowFragments error: &error];
         if(error)
             
             NSLog(@"%@",error.description);
         else
             NSLog(@"Response%@",JSON);
             //dataClass_obj.Dict_object=[JSON valueForKey:@"data"];
         
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
        
             
             //[tblViewMembers reloadData];
             //[self.ActivitIndicator stopAnimating];
             //[self.ActivitIndicator setHidesWhenStopped:YES];
             
         });
         
     }];
    

    
 
}



 -(void)webserviceData
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://hardcastlegis.co.in/Mike_Messenger/userDetail/getUserDetails.php"]];
    [request setHTTPMethod:@"POST"];
    
    NSString *mobileno=[[NSUserDefaults standardUserDefaults]valueForKey:@"PhoneNumber"];
    NSString *userUpdate =[NSString stringWithFormat:@"MOBILE_NUM=%@",mobileno];
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [request setHTTPBody:data1];
    
   [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *resp,NSData *data,NSError *error)
     {
         NSString *txt = [[NSString alloc] initWithData:data encoding: NSASCIIStringEncoding];
       
         
         NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [txt dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingAllowFragments error: &error];
         if(error)
             
             NSLog(@"%@",error.description);
         else
             
             NSLog(@"Response : %@", JSON);
             
             dataClass_obj.Dict_object=[JSON valueForKey:@"data"];
         
        
         
         dispatch_async(dispatch_get_main_queue(), ^{
            
           
            
          NSLog(@"Response : %@", dataClass_obj.Dict_object);
          
             
             
             txtFirstName.text=[[dataClass_obj.Dict_object valueForKey:@"FIRST_NAME"]objectAtIndex:0];
             txtLastName.text=[[dataClass_obj.Dict_object valueForKey:@"LAST_NAME"]objectAtIndex:0];
             txtMobilt.text=[[dataClass_obj.Dict_object valueForKey:@"MOBILE_NUM"]objectAtIndex:0];
             txtOffMobile.text=[[dataClass_obj.Dict_object valueForKey:@"OFFICE_NUM"]objectAtIndex:0];
             txtHomeMobile.text=[[dataClass_obj.Dict_object valueForKey:@"HOME_NUM"]objectAtIndex:0];
             txtOfficeMail.text=[[dataClass_obj.Dict_object valueForKey:@"OFFICE_ADDRESS"]objectAtIndex:0];
             txtPersonalMail.text=[[dataClass_obj.Dict_object valueForKey:@"PERSONAL_EMAIL"]objectAtIndex:0];
             txtOffAddr.text=[[dataClass_obj.Dict_object valueForKey:@"OFFICE_ADDRESS"]objectAtIndex:0];
             txtHomeAddr.text=[[dataClass_obj.Dict_object valueForKey:@"HOME_ADDRESS"]objectAtIndex:0];
             txtMaleFemale.text=[[dataClass_obj.Dict_object valueForKey:@"GENDER"]objectAtIndex:0];
             txtDate.text=[[dataClass_obj.Dict_object valueForKey:@"DOB"]objectAtIndex:0];
             txtMonth.text=[[dataClass_obj.Dict_object valueForKey:@"DOB"]objectAtIndex:0];
             txtYear.text=[[dataClass_obj.Dict_object valueForKey:@"DOB"]objectAtIndex:0];
             
                         //[tblViewMembers reloadData];
             //[self.ActivitIndicator stopAnimating];
             //[self.ActivitIndicator setHidesWhenStopped:YES];
             
         });
         
     }];
    
    
}
    // Do any additional setup after loading the view.

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBack_Click:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"Back";
    
    //[self.parentViewController.navigationController setNavigationBarHidden:YES];
    //[self.parentViewController.navigationController setNavigationBarHidden:YES];
  /* UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = item;
    
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont fontWithName:@"CenturyGothic" size:18.0f], NSFontAttributeName,
                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                       nil]
                             forState:UIControlStateNormal]; */
}




#pragma mark - text field delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //[scrolView setContentOffset:CGPointMake(0, 70)];
   // scrolView.frame = CGRectMake(0, 0, 320, 210);

   /* if([textField isEqual:txtOffMobile])
    {
        //[scrolView setContentOffset:CGPointMake(0, 70)];
       
    }
    if([textField isEqual:txtHomeMobile])
    {
        [scrolView setContentOffset:CGPointMake(0, 70)];
       
    }*/
    
    if([textField isEqual:txtOfficeMail])
    {
        [scrolView setContentOffset:CGPointMake(0, 100)];
       
    }
    if([textField isEqual:txtPersonalMail])
    {
        [scrolView setContentOffset:CGPointMake(0, 100)];
            }
    if([textField isEqual:txtOffAddr])
    {
        [scrolView setContentOffset:CGPointMake(0, 190)];
       
    }
    if([textField isEqual:txtHomeAddr])
    {
        [scrolView setContentOffset:CGPointMake(0, 230)];
        
    }
    if([textField isEqual:txtMaleFemale])
    {
        [scrolView setContentOffset:CGPointMake(0, 260)];
       
    }
   if([textField isEqual:txtDate])
    {
        picker=1;
        [txtMaleFemale resignFirstResponder];
        [txtDate resignFirstResponder];
        [scrolView setContentOffset:CGPointMake(0, 100)];
        
        [picuiview setHidden:NO];
        picuiview.translatesAutoresizingMaskIntoConstraints=YES;
        [picuiview setFrame:CGRectMake(112, 242, 62, 241)];

        //[DOBPicker reloadAllComponents];
    }
   
    if([textField isEqual:txtMonth])
    {
        picker=2;
        [txtMaleFemale resignFirstResponder];
        [txtMonth resignFirstResponder];
       // [scrolView setContentOffset:CGPointMake(0, 100)];
        [picuiview setHidden:NO];
        picuiview.translatesAutoresizingMaskIntoConstraints=YES;
        [picuiview setFrame:CGRectMake(180, 242, 62, 241)];
       
        [DOBPicker reloadAllComponents];
    }
    if([textField isEqual:txtYear])
    {
        picker=3;
        [txtMaleFemale resignFirstResponder];
        [txtYear resignFirstResponder];
        // [scrolView setContentOffset:CGPointMake(0, 100)];
        [picuiview setHidden:NO];
        picuiview.translatesAutoresizingMaskIntoConstraints=YES;
        [picuiview setFrame:CGRectMake(250, 242, 62, 241)];
        
        [DOBPicker reloadAllComponents];
    }
  
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
     [txtMaleFemale resignFirstResponder];
    
    if (picker==1) {
        txtDate.text=[arrDay objectAtIndex:row];
    }else if (picker==2){
         txtMonth.text=[arrMonth objectAtIndex:row];
    }else{
        txtYear.text=[arrYear objectAtIndex:row];
    }
    
    [picuiview setHidden:YES];
   
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    scrolView.frame = CGRectMake(0, 0, 320, 460);
    [textField resignFirstResponder];
    return YES;
}

/*
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y);
    [scrolView setContentOffset:scrollPoint animated:YES];
}
*/

- (void)textFieldDidEndEditing:(UITextField *)textField 
 {
  
     
     if (textField==txtOfficeMail)
     {
         NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
         NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
         //Valid email address
         
         if ([emailTest evaluateWithObject:txtOfficeMail.text] == YES)
         {
             //Do Something
         }
         else if([txtOfficeMail.text isEqualToString:@""])
         {
             
         }else{
             [self showalerview:102];
         }
     }else if (textField==txtPersonalMail)
     {
         NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
         NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
         //Valid email address
         
         if ([emailTest evaluateWithObject:txtPersonalMail.text] == YES)
         {
             //Do Something
         }
         else if([txtPersonalMail.text isEqualToString:@""])
         {
             
         }else{
             [self showalerview:102];
         }
     }
     
     if (textField==txtMobilt) {
         int len=[txtMobilt.text length];
         if (len==10) {
             
         }else if([txtMobilt.text isEqualToString:@""])
         {
             
         }else{
             
             [self showMobieAlert:103];
                      }
     }else if(textField==txtHomeMobile) {
         int len=[txtHomeMobile.text length];
         if (len==10) {
             
         }else if([txtHomeMobile.text isEqualToString:@""])
         {
             
         }else{
             
             [self showMobieAlert:104];
         }
     }else if(textField==txtOffMobile) {
         int len=[txtOffMobile.text length];
         if (len==10) {
             
         }else if([txtOffMobile.text isEqualToString:@""])
         {
             
         }else{
             
             [self showMobieAlert:105];
         }
     }

}

-(void)showMobieAlert:(NSInteger) tagVal
{
    UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"Please enter correct Number"
                                                     message:nil
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
  
    [dialog setTag:tagVal];
    
    [dialog setDelegate:self];
    [dialog show];
 
}
-(void)showalerview:(NSInteger )tagVal;
{
    UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"Please enter correct E-Mail"
                                                     message:nil
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [dialog setDelegate:self];
    [dialog setTag:tagVal];
    [dialog show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
