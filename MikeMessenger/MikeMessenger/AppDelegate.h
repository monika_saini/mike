//
//  AppDelegate.h
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

