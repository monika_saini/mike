//
//  DMActivityInstagram.h
//  DMActivityInstagram
//
//  Created by Cory Alder on 2012-09-21.
//  Copyright (c) 2012 Cory Alder. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DMResizerViewController.h"
//#import "TestResultViewController.h"

@interface DMActivityInstagram : UIActivity < UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) UIImage *shareImage;
@property (nonatomic, strong) NSString *shareString;
@property (nonatomic, strong) NSArray *backgroundColors;
@property (readwrite) BOOL includeURL;

@property (nonatomic, strong) UIBarButtonItem *presentFromButton;
@property (nonatomic, strong) UIButton *anchorButton;
@property (nonatomic, strong) UIView *viewToDisplay;
//@property (nonatomic, strong) TestResultViewController *superViewController;
@property (nonatomic, strong) UIDocumentInteractionController *documentController;


@end
