//
//  CreateTaskCell.h
//  MikeMessenger
//
//  Created by Dipak B on 2/16/15.
//
//

#import <UIKit/UIKit.h>

@interface CreateTaskCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTaskName;
@property (weak, nonatomic) IBOutlet UIButton *btnuncheck;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;


@end
