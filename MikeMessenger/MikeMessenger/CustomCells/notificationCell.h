//
//  notificationCell.h
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import <UIKit/UIKit.h>

@interface notificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblnotify;
@property (weak, nonatomic) IBOutlet UISwitch *onoff;

@end
