//
//  StatusesTableViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/20/15.
//
//

#import <UIKit/UIKit.h>

@interface StatusesTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property(nonatomic,strong)IBOutlet UITableView *tView;


@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;

@end
