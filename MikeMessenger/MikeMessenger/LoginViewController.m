//
//  LoginViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/3/15.
//
//

#import "LoginViewController.h"
#import "FCAPIController.h"
#import "FCAuthFacebookManager.h"
#import "FCRequestFacebookManager.h"
#import "FCUser.h"
#import "ChatListViewController.h"
#import "FCChatDataStoreManager.h"
#import "Sequencer.h"

#define API_ADDUSERDETAILS @"http://hardcastlegis.co.in/Mike_Messenger/userDetail/addUserDetails.php"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize nextButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    
    self.loginOptionArray=[NSMutableArray arrayWithObjects:@"Mike",@"Facebook",@"Google+", nil];
    self.loginIconArray=[NSMutableArray arrayWithObjects:@"mike messenger",@"fb messenger",@"google messenger", nil ];
   
    float lCurrentWidth = self.view.frame.size.width;
    //float  lCurrentHeight = self.view.frame.size.height;
    NSLog(@"lCurrentWidth=%f",lCurrentWidth);
    // Change the size of page view controller
    
    if (lCurrentWidth==320) {
        nextButton.translatesAutoresizingMaskIntoConstraints=YES;
        [nextButton setFrame:CGRectMake(110, 450, 100, 30)];
    }else
    {
        
        nextButton.translatesAutoresizingMaskIntoConstraints=YES;
        [nextButton setFrame:CGRectMake(140, 500, 100, 32)];
        
    }

    
   /* for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"%@", fontName);
        }
    }*/
}
//shows the login view when no data for logged-in user is stored.
/*- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    NSString *login = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    if (!login) {
        
        [self showLogin];
        
    }
}
*/

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton=YES;
    self.title=@"Log In";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - IBActions
-(IBAction)nextPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"pushTab" sender:self];

}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.loginOptionArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MyIdentifier"] ;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
      //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [cell.textLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:20.0]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];

    [cell.textLabel setTextColor:[UIColor grayColor]];
    [cell.detailTextLabel setTextColor:[UIColor colorWithRed:0/255.0 green:179.0/255.0 blue:0/255.0 alpha:1.0f]];

   
    
    // Configure the cell...
    cell.imageView.image=[UIImage imageNamed:self.loginIconArray[indexPath.row]];

    [cell.textLabel setText:self.loginOptionArray[indexPath.row]];
    
    if (indexPath.row==1 || indexPath.row==2) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(0.0, 0.0, cell.frame.size.width/3, 25.0);
        [button.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:18.0]];
        [button.titleLabel setTextAlignment:(NSTextAlignmentRight)];
        [button setTitle:@"Login" forState:(UIControlStateNormal)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];

        [button addTarget:self action:@selector(checkButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = button;
    }else{//mike
        [cell.detailTextLabel setText:@"Confirmed"];

    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    //invoke FB login sequencer
    __weak LoginViewController *self_ = self;
    if(indexPath.row==1){//FB
        [[[FCAPIController sharedInstance] authFacebookManager] authorize];
        [[FCAPIController sharedInstance] authFacebookManager].facebookAuthHandler = ^(NSNumber *sucess, NSError *error){
            if (!error) {
  
                [self_ runSequncerForIndexPath:indexPath];
            }
        };
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self tableView: self.tableView  accessoryButtonTappedForRowWithIndexPath: indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)runSequncerForIndexPath:(NSIndexPath *)indexPath
{
    //__weak LoginViewController *self_ = self;
    Sequencer *sequencer = [Sequencer new];
    [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
        
        [[[FCAPIController sharedInstance] requestFacebookManager] requestGraphMeWithCompletion:^(NSDictionary *response, NSError *error){
            if (!error) {
                FCUser *currentUser = [[FCUser alloc] initWithDict:response];
                [[FCAPIController sharedInstance] setCurrentUser:currentUser];
                
                NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
                
                NSURL * url = [NSURL URLWithString:API_ADDUSERDETAILS];
                NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
                NSString *params = [NSString stringWithFormat:@"FIRST_NAME=%@&LAST_NAME=%@&FACEBOOK_ID=%@&GENDER=%@&DOB=%@&PERSONAL_EMAIL=%@&HOME_ADDRESS=%@&COUNTRY=""&MOBILE_NUM=%@&OFFICE_NUM=""&HOME_NUM=""&OFFICIAL_EMAIL=""&OFFICE_ADDRESS=""&WHATSAPP_ID=""&VIBER_ID=""&WECHAT_ID=""",currentUser.first_name,currentUser.last_name,currentUser.userId,currentUser.gender,currentUser.birthday,currentUser.email,currentUser.location,[[NSUserDefaults standardUserDefaults] objectForKey:@"PhoneNumber"]];
                
                [urlRequest setHTTPMethod:@"POST"];
                [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    NSLog(@"Response:%@ %@\n", response, error);
                    if(error == nil)
                    {
                        NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                        NSLog(@"Data = %@",text);
            
                        
                    }else{
                        
                        //error: timeout
                        
                    }
                    
                    
                } ];
                
                [dataTask resume];

                NSLog(@"This is the first step");
                completion(nil);
            }
        }];
    }];
    
    [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
        
        [[[FCAPIController sharedInstance] requestFacebookManager] requestGraphFriendsWithCompletion:^(NSArray *responseArray, NSError *error) {
            if (!error) {
                [[[FCAPIController sharedInstance] chatDataStoreManager] differenceOfFriendsIdWithNewConversation:responseArray
                                                                                                   withCompletion:^(NSNumber *sucess, NSError *eror){
                                                                                                       if (sucess) {
                                                                                                           NSLog(@"This is second step");
                                                                                                           completion(nil);
                                                                                                       }}];
            }
        }];
    }];
    
    [sequencer enqueueStep:^(id result, SequencerCompletion completion) {
 
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        UIButton *button = (UIButton *)cell.accessoryView;
        [button setTitle:@"Connected" forState:(UIControlStateNormal)];
        [button setTitleColor:[UIColor colorWithRed:0/255.0 green:179.0/255.0 blue:0/255.0 alpha:1.0f] forState:(UIControlStateNormal)];
        [button setUserInteractionEnabled:NO];
        [cell setUserInteractionEnabled:NO];
        
        [self.nextButton setEnabled:YES];
        
        NSLog(@"This is last step");
        completion(nil);
    }];
    
    [sequencer run];
}

@end
