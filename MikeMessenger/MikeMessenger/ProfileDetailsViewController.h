//
//  ProfileDetailsViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/17/15.
//
//

#import <UIKit/UIKit.h>
#import "DataClass.h"

@interface ProfileDetailsViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    DataClass *dataClass_obj;
    __weak IBOutlet UILabel *lblPhone;
    __weak IBOutlet UIScrollView *scrolView;
    
    __weak IBOutlet UITextField *txtOfficeMail;
    
    __weak IBOutlet UITextField *txtPersonalMail;
    
    __weak IBOutlet UIView *picuiview;
    
    __weak IBOutlet UITextField *txtFirstName;
    
    __weak IBOutlet UITextField *txtLastName;
    
    __weak IBOutlet UITextField *txtMobilt;
    __weak IBOutlet UITextField *txtOffMobile;
    
    __weak IBOutlet UITextField *txtHomeMobile;

       float lCurrentWidth;
    __weak IBOutlet UIPickerView *DOBPicker;
    
    
    __weak IBOutlet UITextField *txtOffAddr;
    
    __weak IBOutlet UITextField *txtYear;
    __weak IBOutlet UITextField *txtMonth;
    
    __weak IBOutlet UITextField *txtHomeAddr;
    
    __weak IBOutlet UITextField *txtMaleFemale;
    
    __weak IBOutlet UITextField *txtDate;
    
    
    NSInteger picker;
    
    
    
    
    
}
-(void)webserviceData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *content1WidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *content1HeightConstraint;
@property(nonatomic,retain)NSMutableDictionary *data_Dict;
/**
 Content 2
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *content2HeightConstraint;

/**
 Content 3
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *content3HeightConstraint;
@property(nonatomic,retain)NSMutableArray *arrDay,*arrMonth,*arrYear;
@end
