//
//  CustomBackView.m
//  MikeMessenger
//
//  Created by Monika on 2/17/15.
//
//

#import "CustomBackView.h"

@implementation CustomBackView
@synthesize profilePicImageView;
@synthesize contactNameLabel, statusLabel;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect {
    // Drawing code
 
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    
    UIView *view = nil;
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"CustomBackView"
                                                     owner:self
                                                   options:nil];
    for (id object in objects) {
        if ([object isKindOfClass:[UIView class]]) {
            view = object;
            break;
        }
    }
    
    if (view != nil) {
        _containerView = view;
        [self addSubview:view];
        self.profilePicImageView.layer.borderWidth = 1.0f;
        self.profilePicImageView.layer.borderColor = [UIColor clearColor].CGColor;
        self.profilePicImageView.layer.masksToBounds = NO;
        self.profilePicImageView.clipsToBounds = YES;
        self.profilePicImageView.layer.cornerRadius = 15;
    }
}
*/
+ (id)customView
{
    CustomBackView *customView = [[[NSBundle mainBundle] loadNibNamed:@"CustomBackView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[CustomBackView class]]){
        return customView;
    }
    else
        return nil;
}
@end
