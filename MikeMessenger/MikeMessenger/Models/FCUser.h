//
//  FCUser.h
//  FacebookChat
//
//  Created by Kanybek Momukeev on 7/28/13.
//
//

#import <Foundation/Foundation.h>

@interface FCUser : NSObject

- (id)initWithDict:(NSDictionary *)dict;

@property (nonatomic, strong, readonly) NSString *userId;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *first_name;
@property (nonatomic, strong, readonly) NSString *last_name;
@property (nonatomic, strong, readonly) NSString *gender;
@property (nonatomic, strong, readonly) NSString *birthday;
@property (nonatomic, strong, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSString *location;

@end
