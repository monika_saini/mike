//
//  FCUser.m
//  FacebookChat
//
//  Created by Kanybek Momukeev on 7/28/13.
//
//

#import "FCUser.h"


@implementation FCUser

- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _userId = [dict objectForKey:@"id"];
        _name = [dict objectForKey:@"name"];
        _first_name = [dict objectForKey:@"first_name"];
       _last_name = [dict objectForKey:@"last_name"];
        _gender  = [dict objectForKey:@"gender"];
        _birthday  = [dict objectForKey:@"birthday"];
       _email  = [dict objectForKey:@"email"];
       _location  = [[dict objectForKey:@"location"] objectForKey:@"name"];
       
    }
    return self;
}

@end
