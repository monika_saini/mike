//
//  NewGroupViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/18/15.
//
//

#import "NewGroupViewController.h"
#import "ContactsListViewController.h"

@interface NewGroupViewController ()

@end

@implementation NewGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.groupSubjectTextfield.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.groupSubjectTextfield.layer.borderWidth = 1.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.groupSubjectTextfield.leftView = paddingView;
    self.groupSubjectTextfield.leftViewMode = UITextFieldViewModeAlways;
    
    self.groupIconImageView.layer.borderWidth = 1.0f;
    self.groupIconImageView.layer.borderColor = [UIColor clearColor].CGColor;
    self.groupIconImageView.layer.masksToBounds = NO;
    self.groupIconImageView.clipsToBounds = YES;
    self.groupIconImageView.layer.cornerRadius = 25;
    
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectGroupImage)];
    [singleTap setNumberOfTapsRequired:1];
    [self.groupIconImageView addGestureRecognizer:singleTap];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushContactList"]) {
        ContactsListViewController *contactListVC = (ContactsListViewController *)[segue destinationViewController];
        contactListVC.isGroupChat=YES;
    }
}

-(IBAction)cancelPressed:(UIButton *)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)nextPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"pushContactList" sender:self];
}

-(void)selectGroupImage{

}

#pragma mark - UITextfield delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
