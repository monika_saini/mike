//
//  MikeViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/19/15.
//
//

#import "MikeViewController.h"
#import "NLPDetailViewController.h"

@interface MikeViewController (){
 NSArray *mikePhotos;
}

@end

@implementation MikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    mikePhotos = [NSArray arrayWithObjects:@"movie_icon.png", @"music_icon.png", @"restaurant_icon.png",@"flight_icon.png", nil];
    
    self.msgTextfield.layer.borderWidth = 1.0f;
    self.msgTextfield.layer.borderColor = [UIColor clearColor].CGColor;
    self.msgTextfield.layer.masksToBounds = NO;
    self.msgTextfield.clipsToBounds = YES;
    self.msgTextfield.layer.cornerRadius = 15;

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    self.tabBarController.navigationController.navigationBarHidden=NO;
    self.tabBarController.navigationItem.leftBarButtonItem=nil;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
    self.tabBarController.navigationItem.hidesBackButton=YES;
    self.tabBarController.navigationItem.title=@"Mike";
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification{
   
    NSDictionary* info = [aNotification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view

    CGPoint textFieldOrigin = self.msgTextfield.frame.origin;
    
    CGFloat textFieldHeight = self.msgTextfield.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, textFieldOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height + textFieldHeight);
        
        [self.scrollView setContentOffset:scrollPoint animated:YES];
        
    }
    [UIView commitAnimations];

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
   /* UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    */
  [self.scrollView setContentOffset:CGPointZero animated:YES];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"presentPopUp"]) {
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
       // NLPDetailViewController *NLPDetailVC = segue.destinationViewController;
       // NLPDetailVC.recipeImageName = [mikePhotos[indexPath.section] objectAtIndex:indexPath.row];
        [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
       
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mikePhotos.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{//presentPopUp
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *serviceImageView = (UIImageView *)[cell viewWithTag:100];
    serviceImageView.image = [UIImage imageNamed:[mikePhotos objectAtIndex:indexPath.row]];
    
    return cell;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)sendPressed:(id)sender{


}
@end
