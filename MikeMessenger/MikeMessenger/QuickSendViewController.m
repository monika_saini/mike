//
//  QuickSendViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/19/15.
//
//

#import "QuickSendViewController.h"
#import "Conversation.h"
#import "FCConversationModel.h"
#import "UIImageView+WebCache.h"
#import "TDBadgedCell.h"
#import "XMPP.h"
#import "Message.h"
#import "NSString+Additions.h"
#import "FCChatDataStoreManager.h"
#import "FCAPIController.h"
#import "ChatMessageViewController.h"
#import "FCUser.h"

@interface QuickSendViewController ()
@property (nonatomic, strong) NSMutableArray *conversations;
@property (nonatomic, strong) NSMutableArray *searchResults;
@end

@implementation QuickSendViewController
@synthesize selectedContacts;
@synthesize isBroadcast;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectedContacts=[NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self prepareDataSourceArray];
    
    [self.tView reloadRowsAtIndexPaths:[self.tView indexPathsForVisibleRows]
                      withRowAnimation:UITableViewRowAnimationNone];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}



-(void)prepareDataSourceArray{
    NSArray *allConversations=[[Conversation MR_findAll] mutableCopy];
    if (self.isBroadcast) {//BROADCAST--->all friends
        self.conversations = [NSMutableArray arrayWithArray:allConversations];
        self.navigationItem.title=@"Broadcast";

    }else{//QUICK SEND---->sorted list of chats w.r.t last sent timestamp
        self.conversations = [NSMutableArray array];
        for (Conversation *conv in allConversations) {
            if ([conv.messages count]) {
                [self.conversations addObject:conv];
            }
        }
        NSMutableArray *sortedConvMsgArray = [NSMutableArray array];
        for (Conversation *conv in self.conversations) {
            NSMutableArray *allMessages = [conv.messages allObjects].mutableCopy;
            NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"sentDate" ascending:YES];
            [allMessages sortUsingDescriptors:@[sortByDate]];
            
            NSDictionary *sortedConvMsgDict = [NSDictionary dictionaryWithObjectsAndKeys:[[allMessages lastObject] sentDate],@"lastMsgDate",conv,@"conversation", nil];
            
            [sortedConvMsgArray addObject:sortedConvMsgDict];
        }
        
        NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"lastMsgDate" ascending:NO selector:@selector(compare:)];
        [sortedConvMsgArray sortUsingDescriptors:[NSArray arrayWithObject:sortDesc]];
        
        [self.conversations removeAllObjects];
        for (NSDictionary *dict in sortedConvMsgArray) {
            [self.conversations addObject:[dict objectForKey:@"conversation"]];
        }
        self.navigationItem.title=@"Quick Send";
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushChatUI"]) {
        
        
        ChatMessageViewController *chatMessageVC = (ChatMessageViewController *)[segue destinationViewController];
        chatMessageVC.quickSendContacts = selectedContacts;
        chatMessageVC.isMultipleUserChat = YES;
        chatMessageVC.conversation = nil;
    }
}

#pragma mark - IBAction methods
-(IBAction)createQuickSend{
    [self performSegueWithIdentifier:@"pushChatUI" sender:self];
}

-(IBAction)cancelPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.searchResults count];
        
    } else {
        return self.conversations.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"contactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil )
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
   // [[[cell contentView] subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];

    Conversation *conversation = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        conversation = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        conversation = [self.conversations objectAtIndex:indexPath.row];
    }
    
    
    if([selectedContacts containsObject:conversation])
        [cell setAccessoryType:(UITableViewCellAccessoryCheckmark)];
    else
        [cell setAccessoryType:(UITableViewCellAccessoryNone)];

    //NSLog(@"%@",conversation.lastMessage);
    NSString *url = [[NSString alloc]
                     initWithFormat:@"https://graph.facebook.com/%@/picture",conversation.facebookId];
    [cell.imageView setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType type){}];
    cell.imageView.layer.borderWidth = 1.0f;
    cell.imageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imageView.layer.masksToBounds = NO;
    cell.imageView.clipsToBounds = YES;
    cell.imageView.layer.cornerRadius = 25;
    
    [cell.textLabel setFont:[UIFont fontWithName:@"CenturyGothic-Bold" size:16.0]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:14.0]];

    cell.textLabel.text = conversation.facebookName;
    
    if (!self.isBroadcast) {//QUICK SEND---> show last sent msg
        NSMutableArray *allMessages = [conversation.messages allObjects].mutableCopy;
        NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"sentDate" ascending:YES];
        [allMessages sortUsingDescriptors:@[sortByDate]];
        NSLog(@"%@,%@",[[allMessages objectAtIndex:0] text], [[allMessages lastObject] text]);
        NSString *lastSentMessage=[NSString stringWithFormat:@"%@",[[allMessages lastObject] text]];
        cell.detailTextLabel.text = lastSentMessage;//@"Last message";//conversation.lastMessage;
    }else
        cell.detailTextLabel.text = @"";

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0f;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Conversation *conversation = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView)
         conversation = self.searchResults[indexPath.row];
    else
        conversation = self.conversations[indexPath.row];
    
    UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
        thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedContacts addObject:conversation];
    }
    else
    {
        thisCell.accessoryType = UITableViewCellAccessoryNone;
        [selectedContacts removeObject:conversation];
    }

}




- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
    // NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"lastMessage contains[c] %@", searchText];
    
    // NSPredicate * resultPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1,predicate2,nil]];
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
    
    self.searchResults = [[self.conversations filteredArrayUsingPredicate:resultPredicate] mutableCopy];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end



