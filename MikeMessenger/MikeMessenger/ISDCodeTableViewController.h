//
//  ISDCodeTableViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/11/15.
//
//

#import <UIKit/UIKit.h>
@class ISDCodeTableViewController;
@protocol ISDCodeTableViewControllerDelegate <NSObject>
- (void)ISDCodeTableViewControllerDidCancel:(ISDCodeTableViewController *)controller;
- (void)ISDCodeTableViewControllerDidSave:(ISDCodeTableViewController *)controller withDict:(NSDictionary *)selectedDict;
@end

@interface ISDCodeTableViewController : UITableViewController
@property (nonatomic, weak) id <ISDCodeTableViewControllerDelegate> delegate;

- (IBAction)cancel:(id)sender;
@end
