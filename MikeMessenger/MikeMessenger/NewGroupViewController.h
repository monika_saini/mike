//
//  NewGroupViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/18/15.
//
//

#import <UIKit/UIKit.h>

@interface NewGroupViewController : UIViewController<UITextFieldDelegate>
@property(nonatomic,strong)IBOutlet UIImageView *groupIconImageView;
@property(nonatomic,strong)IBOutlet UITextField *groupSubjectTextfield;

-(IBAction)nextPressed:(UIButton *)sender;
-(IBAction)cancelPressed:(UIButton *)sender;

@end
