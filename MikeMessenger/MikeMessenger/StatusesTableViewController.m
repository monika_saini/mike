//
//  StatusesTableViewController.m
//  MikeMessenger
//
//  Created by Monika on 2/20/15.
//
//

#import "StatusesTableViewController.h"
#import "Conversation.h"
#import "FCConversationModel.h"
#import "UIImageView+WebCache.h"
#import "TDBadgedCell.h"
#import "XMPP.h"
#import "Message.h"
#import "NSString+Additions.h"
#import "FCChatDataStoreManager.h"
#import "FCAPIController.h"
#import "ChatMessageViewController.h"
#import "FCUser.h"

@interface StatusesTableViewController ()
@property (nonatomic, strong) NSMutableArray *conversations;
@property (nonatomic, strong) NSMutableArray *searchResults;
@end

@implementation StatusesTableViewController
@synthesize SearchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [SearchBar setDelegate:self];
    self.conversations = [[Conversation MR_findAll] mutableCopy];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationController.navigationBarHidden=NO;
    self.tabBarController.navigationItem.leftBarButtonItem=nil;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
    self.tabBarController.navigationItem.hidesBackButton=YES;
    self.tabBarController.navigationItem.title=@"Status";
    
    [self.tView reloadRowsAtIndexPaths:[self.tView indexPathsForVisibleRows]
                      withRowAnimation:UITableViewRowAnimationNone];
   
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.searchResults count];
        
    } else {
        return self.conversations.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"statusCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil )
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    Conversation *conversation = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        conversation = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        conversation = [self.conversations objectAtIndex:indexPath.row];
    }
    
    //Conversation *conversation = [self.conversations objectAtIndex:indexPath.row];
  
    //NSLog(@"%@",conversation.lastMessage);
    NSString *url = [[NSString alloc]
                     initWithFormat:@"https://graph.facebook.com/%@/picture",conversation.facebookId];
    [cell.imageView setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType type){}];
    cell.imageView.layer.borderWidth = 1.0f;
    cell.imageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imageView.layer.masksToBounds = NO;
    cell.imageView.clipsToBounds = YES;
    cell.imageView.layer.cornerRadius = 25;
    
    [cell.textLabel setFont:[UIFont fontWithName:@"CenturyGothic-Bold" size:16.0]];
    cell.textLabel.text = conversation.facebookName;
    
    //if (indexPath.row) {
    cell.detailTextLabel.text = @"Status message";//conversation.lastMessage;
    
    /* }else
     cell.detailTextLabel.text = @"Love";//conversation.lastMessage;*/
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0f;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //[self performSegueWithIdentifier:@"showBirthdays" sender:indexPath];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        [self.searchResults removeObjectAtIndex:indexPath.row];
        
    } else {
        [self.conversations removeObjectAtIndex:indexPath.row];
    }
    // Request table view to reload
    [self.tView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    if (self.tView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. The view for the header
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    // 2. Set a custom background color and a border
    headerView.backgroundColor = [UIColor whiteColor];
    //headerView.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1.0].CGColor;
   // headerView.layer.borderWidth = 1.0;
    
    UIButton *birthdayButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [birthdayButton setBackgroundColor:[UIColor clearColor]];
    [birthdayButton.titleLabel setFont:[UIFont fontWithName:@"CenturyGothic" size:12.0]];
    [birthdayButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [birthdayButton addTarget:self action:@selector(moveToBirthday) forControlEvents:UIControlEventTouchUpInside];
    [birthdayButton setTitle:@"XYZ and N other have birthday today" forState:UIControlStateNormal];
    
    
    
    
    // 4. Add the label to the header view
    [headerView addSubview:birthdayButton];
    
    
    // 5. Finally return
    return headerView;
}
#pragma mark - Helper methods
-(void)moveToBirthday{
    [self performSegueWithIdentifier:@"showBirthdays" sender:self];
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
    // NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"lastMessage contains[c] %@", searchText];
    
    // NSPredicate * resultPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1,predicate2,nil]];
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"facebookName contains[c] %@", searchText];
    
    self.searchResults = [[self.conversations filteredArrayUsingPredicate:resultPredicate] mutableCopy];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end

