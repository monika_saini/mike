//
//  TaskViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/16/15.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CreateTaskCell.h"
#import <QuartzCore/QuartzCore.h>
#import <EventKit/EventKit.h>
#import "DMActivityInstagram.h"

@interface TaskViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSString *path;
    __weak IBOutlet UITableView *tblTask;
    __weak IBOutlet UIButton *Share;
    __weak IBOutlet UITextField *txtCreateTask;
    
    __weak IBOutlet UIButton *btnDone;
   // __weak IBOutlet UIView *dateView;
    
    
    __weak IBOutlet UIButton *btnRemindme;
    __weak IBOutlet UIButton *btnMorning;
    
    int leftOffsetDatePicker;
    __weak IBOutlet UIButton *btnClearTask;
    __weak IBOutlet UIBarButtonItem *btnShare;
    
    __weak IBOutlet UIDatePicker *datePicker;
    NSInteger c,add,clear;
    NSDate *bookingDate;
    int cnt;
    int noofTask;
    int t;
    __weak IBOutlet UIButton *btnSelectTime;
    __weak IBOutlet UIButton *btnEvening;
   
    __weak IBOutlet UIButton *btnnight;
    __weak IBOutlet UIButton *btnAfternoon;
    
    __weak IBOutlet UIView *timeView;
    __weak IBOutlet UIButton *btnTomorrow;
    __weak IBOutlet UIButton *btnToday;
    
    
    __weak IBOutlet UIButton *btnPickerDobe;
   
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UIView *selectdateview;
    __weak IBOutlet UIButton *btnmor;
    __weak IBOutlet UIButton *btnSelectDate;
    NSDate *date;
    NSString *Stringdate;
    NSString *Strtime;
    NSInteger cntarrtask;
    NSInteger val;
    NSInteger arrtaskval;
}
@property(nonatomic,retain)NSMutableArray *arrTask,*arrCompletedTaskCount,*arrdublicateTask;
@property(nonatomic,retain)NSMutableArray *arrchk;
@property(nonatomic,retain)NSMutableArray *arrData,*arrdumytask;
@property (strong, nonatomic) EKEventStore *eventStore;
@property (weak, nonatomic) IBOutlet UIView *calenderView;
@property (nonatomic, strong) UIPopoverController *popover;
-(void)setEvent;
-(void)openmailmessage;
@end
