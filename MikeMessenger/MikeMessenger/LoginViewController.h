//
//  LoginViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/3/15.
//
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSMutableArray *loginOptionArray;
@property(nonatomic,strong)NSMutableArray *loginIconArray;
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)IBOutlet UIButton *nextButton;

-(IBAction)nextPressed:(UIButton *)sender;

@end
