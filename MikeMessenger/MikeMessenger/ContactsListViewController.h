//
//  ContactsListViewController.h
//  MikeMessenger
//
//  Created by Monika on 2/16/15.
//
//

#import <UIKit/UIKit.h>


@interface ContactsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    
    __weak IBOutlet UISearchBar *searchBar;
    NSArray *filtered;
    NSArray *contactsInSection;
}
@property(nonatomic,assign) BOOL isGroupChat;
@property(nonatomic,strong)IBOutlet UITableView *tView;
- (IBAction)cancel:(id)sender;
- (IBAction)nextPressed:(id)sender;

@property (nonatomic, copy) NSMutableArray *friendsList;
@property (nonatomic, strong) NSMutableArray *selectedContacts;


-(IBAction)createNewGroup;
-(IBAction)createBroadcast;
@end
