//
//  AppDelegate.m
//  MikeMessenger
//
//  Created by Monika on 1/29/15.
//
//

#import "AppDelegate.h"
#import "XMPP.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "FCAPIController.h"
#import "FCChatDataStoreManager.h"
#import "FCAuthFacebookManager.h"
#import "LoginViewController.h"
#import "ChatListViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"MyDatabase.sqlite"];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:@"SMS_Verified"]!=nil) {//sms verified
        if ([userDefaults valueForKey:@"fbToken"]!=nil) {//login token available, move to chat list
            UITabBarController *tabBarcontroller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]  instantiateViewControllerWithIdentifier:@"tabBarcontroller"];
            UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:tabBarcontroller];

            self.window.rootViewController=navController;

        }else{//no login token, move to login
            LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
            UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:loginController];
            self.window.rootViewController=navController;
        }
    }else{//first time launch, move to default master(guide) screen
        [userDefaults setObject:nil forKey:@"SMS_Verified"];
        [userDefaults synchronize];
    }
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];

    //[application setStatusBarStyle:UIStatusBarStyleLightContent];

    //customize background-color of navigation bar
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:32.0/255.0 green:74.0/255.0 blue:104.0/255.0 alpha:1.0f]];
    //customize color of back button
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
   
    /*[[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor],
      NSForegroundColorAttributeName,
      [UIColor whiteColor],
      NSForegroundColorAttributeName,
      [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
      NSForegroundColorAttributeName,
      [UIFont fontWithName:@"CenturyGothic" size:18.0],
      NSFontAttributeName,
      nil]];*/
    
     /*[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
     //[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIFont fontWithName:@"CenturyGothic" size:18.0]}];
    
   //customize back-button image
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_btn.png"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_btn.png"]];*/

    //customize navigation bar title
   /* NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);*/
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                           /*shadow, NSShadowAttributeName,*/
                                                           [UIFont systemFontOfSize:18.0], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setTranslucent:NO];

    
    //customize navigation bar-button item
    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName : [UIFont systemFontOfSize:18.0], NSForegroundColorAttributeName: [UIColor whiteColor]};//[UIFont fontWithName:@"CenturyGothic" size:20.0]
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
/*  
    //use image as navigationItem title
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appcoda-logo.png"]];
 
    //adding multiple right bar-button items
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:nil];
    UIBarButtonItem *cameraItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:nil];
    
    NSArray *actionButtonItems = @[shareItem, cameraItem];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
*/
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];

    /*
     // Change the tab bar background
     UIImage *tabBarBackground = [UIImage imageNamed:@"CustomUITabbar.png"];
     [[UITabBar appearance] setBackgroundImage:tabBarBackground];
     */
    

    return YES;
}

/*
//changing the status-bar style per VC
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
 //Alternatively, you can set the status bar style by using the UIApplication statusBarStyle method. But first you’ll need to opt out the “View controller-based status bar appearance”. Under the Info tab of the project target, insert a new key named “View controller-based status bar appearance” and set the value to NO
 //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

}
 
//hiding the status-bar style per VC
- (BOOL)prefersStatusBarHidden
{
 return YES;
}
*/
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[[FCAPIController sharedInstance] chatDataStoreManager] saveContext];

}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [[[FCAPIController sharedInstance] authFacebookManager] handleOpenURL:url];
}


@end
