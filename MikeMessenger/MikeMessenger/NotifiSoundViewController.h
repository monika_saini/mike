//
//  NotifiSoundViewController.h
//  MikeMessenger
//
//  Created by Dipak B on 2/18/15.
//
//

#import <UIKit/UIKit.h>
#import "notificationCell.h"

@interface NotifiSoundViewController : UIViewController
{
    
    __weak IBOutlet UITableView *tblNotify;
}
@property(nonatomic,retain)NSMutableArray *arrData;
@end
